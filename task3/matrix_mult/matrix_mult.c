#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <mpi.h>

#define DIM 4 /* Dimension of matrices */
#define RANGE 10 /* Range of random generator */
#define MASTER 0 /* Rank of master process */

/* Convert two dimensional to one dimensional coordinates */
int convert_i(int i, int j, int dimension)
{
	return dimension * i + j;
}

/* Generate a random matrix */
int *generate_m(int dimension, int range)
{
	int *m = malloc (sizeof(int) * dimension * dimension);

	for (size_t i = 0; i < dimension; i++) {
		for (size_t j = 0; j < dimension; j++) {
			m[dimension * i + j] = rand() % range;
		}
	}

	return m;
}

/* Levitation device creating zero-gravity ...oh wait it's just a print method */
void print_m(int *m, int rank, int dimension)
{
	//printf("I am %d\n", rank);
	for (size_t i = 0; i < dimension; i++) {
		for (size_t j = 0; j < dimension; j++) {
			printf("%3d ", m[dimension * i + j]);
		}
		printf("\n");
	}
}

/* Partially multiply matrices */
int *multiply_m(int *m1, int *m2, int c_begin, int c_end, int dimension)
{
	int *m3 = malloc(sizeof(int) * dimension * dimension);

	for (size_t i = c_begin; i <= c_end; i++) {
		for (size_t j = 0; j < dimension; j++) {
			int sum = 0;
			for (size_t k = 0; k < dimension; k++) {
				sum += m1[dimension * i + k] * m2[dimension * k + j];
			}
			m3[dimension * i + j] = sum;
		}
	}

	return m3;
}

int main(int argc, char **argv)
{
	srand(time(NULL));
	clock_t start;
	clock_t end;
	
	int rank; /* Rank of this process */
	int p_num; /* Total number of processes */
	int c_size; /* Number of rows for each process */
	int c_begin; /* Start index for process*/
	int c_end; /* End index for processes*/

	int dimension;
	int range;

	if(argc > 2) {
		dimension = atoi(argv[1]);
		range = atoi(argv[2]);
	} else {
		dimension = DIM;
		range = RANGE;
	}

	int *m1 = malloc (sizeof(int) * dimension * dimension);
	int *m2 = malloc (sizeof(int) * dimension * dimension);
	int *m3 = malloc(sizeof(int) * dimension * dimension);

	MPI_Init (&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &p_num);

	if ((dimension % p_num) != 0) {
		printf("Could not distribute rows evenly.");
		exit(-1);
	}

	c_size = dimension / p_num;

	c_begin = rank * c_size;
	c_end = c_begin + c_size - 1;

	if (rank == MASTER) {
		m1 = generate_m(dimension, range);
		m2 = generate_m(dimension, range);
		start = clock();
	}

	/* Broadcast values of second matrix to all processes */
	MPI_Bcast(m2, dimension * dimension, MPI_INT, MASTER, MPI_COMM_WORLD);

	/* Scatter chunks of first matrix to all other processes */
	MPI_Scatter(m1, dimension * c_size, MPI_INT, &m1[convert_i(c_begin, 0, dimension)], dimension * c_size, MPI_INT, MASTER, MPI_COMM_WORLD);

	/* Execute partial multiplication */
	m3 = multiply_m(m1, m2, c_begin, c_end, dimension);

	/* Collect all results and gather in m3 */
	MPI_Gather(&m3[convert_i(c_begin, 0, dimension)], dimension * c_size, MPI_INT, m3, dimension * c_size, MPI_INT, MASTER, MPI_COMM_WORLD);

	/* Print final result */
	if(rank == MASTER) {
		end = clock();
    		printf( "Time: %f\n", (end-start)/(double)CLOCKS_PER_SEC);
		//print_m(m3, rank, dimension);
	}

	MPI_Finalize();

	return EXIT_SUCCESS;
}
