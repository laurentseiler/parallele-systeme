#include "cmdline_args.h"

static void print_usage(char *progname, char option)
{
	if (option)
		printf("Bad arguments for option -%c!\n", option);

	printf("\nUsage:\n"
	       "%s [-n num] [-i num] [-s num]\n"
	       "\n"
	       "-n num    Number of nodes in graph (e.g. 1000)\n"
	       "-i num    Interconnection of nodes in percent (e.g. 0.1)\n"
	       "-s num    Starting node (e.g. 0)\n",
	       progname);

	exit(EXIT_FAILURE);
}

void parse_cmdline_args(struct graph *g, int argc, char **argv)
{
	int c;

	// Init struct with default values
	g->num_nodes = 16000;
	g->interconnection = 0.01;
	g->start_node = 0;
	g->current_node = 0;

	// Parse command-line args
	while ((c = getopt(argc, argv, "n:i:s:")) != -1) {
		switch (c) {
		case 'n':
			if (atoi(optarg))
				g->num_nodes = atoi(optarg);
			else
				print_usage(argv[0], 'n');
			break;
		case 'i':
			if (atof(optarg))
				g->interconnection = atof(optarg);
			else
				print_usage(argv[0], 'f');
			break;
		case 's':
			if (atoi(optarg))
				g->start_node = atoi(optarg);
			else
				print_usage(argv[0], 's');
			break;
		case '?':
		default:
			print_usage(argv[0], 0);
		}
	}

	for (int i = optind; i < argc; i++) {
		fprintf(stderr, "Warning: Unknown argument %s!\n", argv[i]);
	}
}

