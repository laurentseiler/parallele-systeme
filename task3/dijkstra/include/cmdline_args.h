#ifndef CMDLINE_ARGS_H
#define CMDLINE_ARGS_H

#include "common.h"

void parse_cmdline_args(struct graph *g, int argc, char **argv);

#endif // CMDLINE_ARGS_H
