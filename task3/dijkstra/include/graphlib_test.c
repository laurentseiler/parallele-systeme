#include "graphlib.h"

static int pos(int from, int to)
{
	if (from > to) {
		int tmp = from;
		from = to;
		to = tmp;
	}
	return 8 * from + to;
}

int get_dist(struct graph *g, int from, int to)
{
	// Something we always know
	if (from == to)
		return 0;

	// Look it up!
	return g->edges_test[pos(from, to)];
}

void update_dist(struct graph *g, int from, int to, int dist)
{
	// The usual check
	if (from == to)
		return;

	// Update
	printf("Inserting dist=%d at %c -- %c\n", dist, from + 65, to + 65);
	g->edges_test[pos(from, to)] = dist;
}

void generate_graph(struct graph *g)
{
	g->edges_test = malloc(8 * 8 * sizeof(int));

	for (int i = 0; i < g->num_nodes; i++) {
		for (int j = 0; j < g->num_nodes; j++) {
			g->edges_test[pos(i, j)] = INT_MAX;
		}
	}

	g->edges_test[pos(0, 1)] = 1;
	g->edges_test[pos(0, 2)] = 4;
	g->edges_test[pos(1, 2)] = 2;
	g->edges_test[pos(1, 6)] = 4;
	g->edges_test[pos(1, 7)] = 2;
	g->edges_test[pos(2, 3)] = 1;
	g->edges_test[pos(2, 4)] = 3;
	g->edges_test[pos(3, 4)] = 1;
	g->edges_test[pos(3, 5)] = 3;
	g->edges_test[pos(3, 6)] = 1;
	g->edges_test[pos(4, 5)] = 1;
	g->edges_test[pos(5, 6)] = 6;
	g->edges_test[pos(6, 7)] = 14;

	g->num_nodes = 8;
}

void print_graph(struct graph *g) {
	if (g->rank != 0)
		return;

	int dist;
	char to_name = 'A';

	printf("\n");
	// printf("Target node: %c\n\n", g->start_node + 65);
	printf("     A     B     C     D     E     F     G     H\n");

	for (int i = 0; i < g->num_nodes; i++) {
		printf("%c   ", to_name++);

		for (int j = 0; j < g->num_nodes; j++) {
			dist = get_dist(g, i, j);
			if (dist != INT_MAX)
				printf("%2d    ", dist);
			else
				printf("      ");
		}
		printf("\n");
	}

	printf("\n");
}

void free_graph(struct graph *g)
{
	free(g->edges_test);
}

