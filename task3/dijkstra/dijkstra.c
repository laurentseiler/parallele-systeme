#include "common.h"

#define MYMIN_MSG 0
#define OVRLMIN_MSG 1
#define COLLECT_MSG 2

struct min {
	int node, dist;
};

struct min min_dist(struct graph *g, bool *computed_vertices)
{
	struct min mymin = {g->begin, INT_MAX};

	int dist_to_start = get_dist(g, g->start_node, g->current_node);

	if (dist_to_start == INT_MAX)
		return mymin;

	for (int to = g->begin; to < g->end; to++) {
		int this_dist = get_dist(g, g->current_node, to);

		if (this_dist < mymin.dist && !computed_vertices[to]) {
			mymin.dist = this_dist + dist_to_start;
			mymin.node = to;
		}
	}

	return mymin;
}

struct min overall_min_dist(struct graph *g, bool *computed_vertices, struct min *mymin)
{
	MPI_Status status;
	struct min overallmin = {10, 11};


	if (g->rank > 0){
		MPI_Send(mymin, 2, MPI_INT, 0, MYMIN_MSG, MPI_COMM_WORLD);
	}else {
		// set own first
		overallmin.node = mymin->node;
		overallmin.dist = mymin->dist;

		struct min othermin ={15,16};
		// check if values need to be replaced
		for(int i = 1; i < g->size; i++){
			MPI_Recv(&othermin, 2, MPI_INT, i, MYMIN_MSG, MPI_COMM_WORLD, &status);

			if (othermin.dist < overallmin.dist){
				overallmin.dist = othermin.dist;
				overallmin.node = othermin.node;
			}
		}
	}

	// disseminating new min distance
	if (g->rank > 0){
		MPI_Recv(&overallmin, 2, MPI_INT, 0, OVRLMIN_MSG, MPI_COMM_WORLD, &status);
	}else{
		for (int i = 1; i < g->size; i++){
			MPI_Send(&overallmin, 2, MPI_INT, i, OVRLMIN_MSG, MPI_COMM_WORLD);
		}
	}

	computed_vertices[overallmin.node] = true;
	g->current_node = overallmin.node;

	return overallmin;
}

void updatemymin(struct graph *g, bool *computed_vertices, struct min *overallmin){

	int from = g->start_node;
	int curr_node = g->current_node;

	for (int to = g->begin; to < g->end; to++){
		int old_dist = get_dist(g, from, to);
		int new_dist_1 = overallmin->dist;
		int new_dist_2 = get_dist(g, curr_node, to);
		int new_dist = new_dist_1 + new_dist_2;

		if (!computed_vertices[to] &&
		    new_dist_1 != INT_MAX &&
		    new_dist_2 != INT_MAX &&
		    new_dist < old_dist)
		{
			// We found a new shortest path!
			update_dist(g, from, to, new_dist);
		}
	}
}

void dowork(struct graph *g)
{
	// Remember which nodes we have already visited
	bool computed_vertices[g->num_nodes];
	memset(computed_vertices, 0, sizeof computed_vertices);

	computed_vertices[0] = true;

	for(int i = 0; i < g->num_nodes; i++){
		struct min mymin = min_dist(g, computed_vertices);
		struct min overallmin = overall_min_dist(g, computed_vertices, &mymin);
		updatemymin(g, computed_vertices, &overallmin);
	}

	// Communicate all data structures to master
	MPI_Status status;

	if (g->rank > 0){
		int chunk_size = g->end - g->begin;
		struct min *finalmin = malloc(chunk_size * sizeof(struct min));
		for (int i = 0; i < chunk_size; i++){
			finalmin[i].node = i + g->begin;
			finalmin[i].dist = get_dist(g, g->start_node, i + g->begin);
		}
		MPI_Send(finalmin, 2 * chunk_size, MPI_INT, 0, COLLECT_MSG, MPI_COMM_WORLD);
		free(finalmin);
	}else{
		struct min *finalmin;
		for (int i = 1; i < g->size; i++){
			int begin = (g->num_nodes / g->size) * i;
			int end = (g->num_nodes / g->size) + begin;
			if (i == g->size - 1)
				end = g->num_nodes;
			int chunk_size = end - begin;

			finalmin = malloc(chunk_size * sizeof(struct min));

			MPI_Recv(finalmin, 2 * chunk_size, MPI_INT, i, COLLECT_MSG, MPI_COMM_WORLD, &status);

			for (int j = 0; j < chunk_size; j++){
				update_dist(g, g->start_node, begin + j, finalmin[j].dist);
			}
			free(finalmin);
		}
	}

}

double measure_time(void func(struct graph *), struct graph *g)
{
	double start, end;

	if (g->rank == 0)
		start = MPI_Wtime();

	func(g);

	if (g->rank == 0)
		end = MPI_Wtime();

	return (end - start);
}

void init(struct graph *g)
{
	g->begin = (g->num_nodes / g->size) * g->rank;
	g->end = (g->num_nodes / g->size) + g->begin;

	if (g->rank == g->size - 1)
		g->end = g->num_nodes;
}

int main(int argc, char **argv)
{
	struct graph g;

	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD, &g.size);
	MPI_Comm_rank(MPI_COMM_WORLD, &g.rank);

	double runtime;

	srand(time(NULL));
	parse_cmdline_args(&g, argc, argv);

	if (g.rank == 0)
		printf("Generating graph. Will need approx %.1f Mb of memory...\n",
		       (g.size * sizeof(struct edge) * g.num_nodes * g.num_nodes * g.interconnection) / 1000000);
	generate_graph(&g);
	print_graph(&g);

	if (g.rank == 0)
		printf("Initializing MPI workers...\n");
	init(&g);

	if (g.rank == 0)
		printf("Executing Dijkstra...\n");
	runtime = measure_time(dowork, &g);

	print_graph(&g);
	free_graph(&g);
	if (g.rank == 0)
		printf("Done in %.3f seconds.\n", runtime);

	MPI_Finalize();

	return EXIT_SUCCESS;
}
