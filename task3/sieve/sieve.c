#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include <string.h>
#include <time.h>

//init array with values and set 1 to 0 as well since it's not prime
void init_array(int *array, int size){
	for(int i=0; i<size; i++){
		array[i] = i;
	}
	array[1] = 0;
}

//inital sieving from 1 - sqrt of task_size (N)
void init_sieve(int* array, int N){
	for(int i=2; i<N; i++){
		for(int k=2, j=i*k; j<N; j=j*++k){
			array[j] = 0;
		}
	}
}

//checks if a[i] is a multiple of a value in 1-N
void sieve(int* array, int N, int start, int end){
	for (int i=start; i<end; i++){
		for (int j=2; j<N; j++){
			if (array[j] != 0 && (array[i] % array[j] == 0)){
				array[i] = 0;
			}
		}
	}
}

int main(int argc, char **argv){

	int N, task_size;
	clock_t t_0, t_e;

	if(argc < 2){
		fprintf(stderr, "no tasksize given!\n");
		return EXIT_FAILURE;
	}


	task_size = atoi(argv[1]);
	N = sqrt(task_size); //initial sieving size

	int* values = malloc(task_size * sizeof(int));
	int* buff = malloc(task_size * sizeof(int));

	init_array(values, task_size);

	init_sieve(values, N);
	
	MPI_Init(&argc, &argv);
	
	int np, rank, step, start, tag=42;

	MPI_Comm_size(MPI_COMM_WORLD, &np);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	//calculate how much work each process has to do
	step = (task_size-N) / np;


  //index where each process has to check for multiples of 1 to squrt of task_size
	start = N+1+rank*step;

	//do actual work
	t_0 = clock();
	sieve(values, N, start, start+step);

	if(rank == 0){
		//gather data
		for(int i=1; i<np; i++){
			memset(buff, 0, task_size * sizeof(int));
			MPI_Recv(buff, task_size * sizeof(int), MPI_BYTE, i, tag,
						MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			int k = N+1+i*step;
			for (int j = k; j < k+step; j++){
				values[j] = buff[j];
			}

		}

		t_e = clock();
		printf("total time %fs\n", (t_e - t_0 * 1.0) / CLOCKS_PER_SEC);

	}else{
		//send sieved values
		MPI_Send(values, task_size * sizeof(int), MPI_BYTE, 0, tag, MPI_COMM_WORLD);
	}

	//finalize
	free(values);
	free(buff);

	MPI_Finalize();

	return EXIT_SUCCESS;
}
