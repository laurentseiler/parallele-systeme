#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

#define swap(x, y) do { \
	size_t tmp = array[x]; \
	array[x] = array[y]; \
	array[y] = tmp; } while(0)

int *sort_gen_array(size_t length, int range)
{
	int *array = malloc(sizeof(int) * length);

	for (size_t i = 0; i < length; i++)
		array[i] = rand() % range;

	return array;
}

void sort_print(int *array, size_t length)
{
	if (length > 50)
		return;

	for (size_t i = 0; i < length; i++)
		printf(" %d", array[i]);
	printf("\n");
}

void quicksort_exec(int *array, size_t length, int parallel_count)
{
	if (length < 2)
		return;

	// Partitioning
	size_t i = -1;
	size_t j = length;
	int pivot = array[length / 2];

	for(;;) {
		do
			i++;
		while (array[i] < pivot);

		do
			j--;
		while (array[j] > pivot);

		if (i < j)
			swap(i, j);
		else
			break;
	}

	// Limit the number of created threads during recursive calls
	if (parallel_count < omp_get_max_threads()) {
		parallel_count *= 2;
		#pragma omp parallel
		#pragma omp sections nowait
		{
			#pragma omp section
			quicksort_exec(array, i, parallel_count);
			#pragma omp section
			quicksort_exec(array + i, length - i, parallel_count);
		}
	} else {
		quicksort_exec(array, i, parallel_count);
		quicksort_exec(array + i, length - i, parallel_count);
	}
}

void quicksort(int *array, size_t length)
{
	omp_set_nested(1);
	quicksort_exec(array, length, 1);
}

int is_correctly_sorted(int *array, size_t length)
{
	for (size_t i = 0; i < length - 1; i++)
		if (array[i] > array[i + 1])
			return 0;
	return 1;
}

void measure_time(void (*func)(int *, size_t), int *arr, size_t len)
{
	double start, end;

	printf("%lu elements, %d threads, starting...", len, omp_get_max_threads());
	fflush(stdout);

	start = omp_get_wtime();
	func(arr, len);
	end = omp_get_wtime();

	printf("Finished in %.3f seconds. ", end - start);

	if (is_correctly_sorted(arr, len))
		printf("Ordering: OK\n");
	else
		printf("Ordering: WRONG!\n");
}

int main(int argc, char *argv[])
{
	int *arr;
	int range = 1000;
	size_t len = 100000000;
	if (argc > 1 && atol(argv[1]) > 0)
		len = atol(argv[1]);

	srand(omp_get_wtime());

	arr = sort_gen_array(len, range);
	sort_print(arr, len);
	measure_time(quicksort, arr, len);
	sort_print(arr, len);
	free(arr);

	return EXIT_SUCCESS;
}

