#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <omp.h>

#define swap(x, y) { \
	tmp = array[x]; \
	array[x] = array[y]; \
	array[y] = tmp; }

int *sort_gen_array(size_t length, int range)
{
	int *array = malloc(sizeof(int) * length);

	for (size_t i = 0; i < length; i++)
		array[i] = rand() % range;

	return array;
}

void sort_print(int *array, size_t length)
{
	if (length > 50)
		return;

	for (size_t i = 0; i < length; i++)
		printf(" %d", array[i]);
	printf("\n");
}

void get_chunk(size_t length, size_t start, int *from, int *to)
{
	int tid = omp_get_thread_num();
	int total = omp_get_num_threads();
	int chunk_sz = (length - start) / total;
	*from = start + (tid * chunk_sz);
	*to = start + ((tid + 1) * chunk_sz);
	if (tid == total - 1)
		*to = length;
}

struct min { size_t idx; int val; };

void selectionsort(int *array, size_t length)
{
	struct min global_min;

	for (size_t i = 0; i < length; i++) {
		global_min.idx = 0;
		global_min.val = INT_MAX;

		#pragma omp parallel
		{
			struct min my_min;
			int from, to;

			#pragma omp for
			for (int t = 0; t < omp_get_max_threads(); t++) {
				get_chunk(length, i, &from, &to);

				// Search for local best
				my_min.idx = from;
				my_min.val = array[from];
				for (size_t j = from; j < to; j++) {
					if (array[j] < my_min.val) {
						my_min.idx = j;
						my_min.val = array[j];
					}
				}
			}

			#pragma omp critical
			if (my_min.val < global_min.val) {
				global_min.idx = my_min.idx;
				global_min.val = my_min.val;
			}

			#pragma omp barrier

			#pragma omp single
			{
				size_t tmp = array[i];
				array[i] = global_min.val;
				array[global_min.idx] = tmp;
			}
		}
	}
}


int is_correctly_sorted(int *array, size_t length)
{
	for (size_t i = 0; i < length - 1; i++)
		if (array[i] > array[i + 1])
			return 0;
	return 1;
}

void measure_time(void (*func)(int *, size_t), int *arr, size_t len)
{
	double start, end;

	printf("%lu elements, %d threads, starting...", len, omp_get_max_threads());
	fflush(stdout);

	start = omp_get_wtime();
	func(arr, len);
	end = omp_get_wtime();

	printf("Finished in %.3f seconds. ", end - start);

	if (is_correctly_sorted(arr, len))
		printf("Ordering: OK\n");
	else
		printf("Ordering: WRONG!\n");
}

int main(int argc, char *argv[])
{
	int *arr;
	size_t len = 120000;
	int range = 1000;
	if (argc > 1 && atol(argv[1]) > 0)
		len = atol(argv[1]);

	srand(omp_get_wtime());

	arr = sort_gen_array(len, range);
	measure_time(selectionsort, arr, len);
	sort_print(arr, len);
	free(arr);

	return EXIT_SUCCESS;
}
