# Parallel Systems - Homework 7

## Usage
Copy all the files to the LCC. On the server, just execute `./run_all.sh`,
which will compile the sources and add the SGE scripts to the queue.

When all computations have finished (this takes several hours, check with
`qstat -u $USER`), copy the `output/` folder back to your local machine.

The files in output now contain individual measurement data. To add speedup
and efficiency measurements and generate this documentation, simply execute
`./generate_readme.sh > README.md`.


## Performance measurement results

### Bubble Sort Bad
```
Problem size: 100000
1 threads, 80.663 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 182.633 seconds. Speedup: 0.441, Efficiency: 0.220
4 threads, 297.796 seconds. Speedup: 0.270, Efficiency: 0.067
8 threads, 354.123 seconds. Speedup: 0.227, Efficiency: 0.028

Problem size: 150000
1 threads, 181.670 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 412.724 seconds. Speedup: 0.440, Efficiency: 0.220
4 threads, 642.257 seconds. Speedup: 0.282, Efficiency: 0.070
8 threads, 759.102 seconds. Speedup: 0.239, Efficiency: 0.029

Problem size: 200000
1 threads, 323.362 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 733.206 seconds. Speedup: 0.441, Efficiency: 0.220
4 threads, 1200.237 seconds. Speedup: 0.269, Efficiency: 0.067
8 threads, 1357.493 seconds. Speedup: 0.238, Efficiency: 0.029

Problem size: 250000
1 threads, 504.443 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 864.580 seconds. Speedup: 0.583, Efficiency: 0.291
4 threads, 1878.169 seconds. Speedup: 0.268, Efficiency: 0.067
8 threads, 2060.264 seconds. Speedup: 0.244, Efficiency: 0.030

```

### Bubble Sort
```
Problem size: 100000
1 threads, 48.328 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 24.469 seconds. Speedup: 1.975, Efficiency: 0.987
4 threads, 13.236 seconds. Speedup: 3.651, Efficiency: 0.912
8 threads, 7.690 seconds. Speedup: 6.284, Efficiency: 0.785

Problem size: 150000
1 threads, 108.423 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 54.569 seconds. Speedup: 1.986, Efficiency: 0.993
4 threads, 29.290 seconds. Speedup: 3.701, Efficiency: 0.925
8 threads, 16.916 seconds. Speedup: 6.409, Efficiency: 0.801

Problem size: 200000
1 threads, 192.926 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 97.144 seconds. Speedup: 1.985, Efficiency: 0.992
4 threads, 51.860 seconds. Speedup: 3.720, Efficiency: 0.930
8 threads, 29.481 seconds. Speedup: 6.544, Efficiency: 0.818

Problem size: 250000
1 threads, 301.322 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 151.747 seconds. Speedup: 1.985, Efficiency: 0.992
4 threads, 80.712 seconds. Speedup: 3.733, Efficiency: 0.933
8 threads, 45.617 seconds. Speedup: 6.605, Efficiency: 0.825

```

### Bucket Sort
```
Problem size: 150000000
1 threads, 43.845 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 26.703 seconds. Speedup: 1.641, Efficiency: 0.820
4 threads, 17.769 seconds. Speedup: 2.467, Efficiency: 0.616
8 threads, 13.658 seconds. Speedup: 3.210, Efficiency: 0.401

Problem size: 200000000
1 threads, 59.583 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 36.691 seconds. Speedup: 1.623, Efficiency: 0.811
4 threads, 24.182 seconds. Speedup: 2.463, Efficiency: 0.615
8 threads, 18.566 seconds. Speedup: 3.209, Efficiency: 0.401

Problem size: 250000000
1 threads, 75.001 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 45.691 seconds. Speedup: 1.641, Efficiency: 0.820
4 threads, 30.273 seconds. Speedup: 2.477, Efficiency: 0.619
8 threads, 23.101 seconds. Speedup: 3.246, Efficiency: 0.405

Problem size: 300000000
1 threads, 91.225 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 55.841 seconds. Speedup: 1.633, Efficiency: 0.816
4 threads, 36.902 seconds. Speedup: 2.472, Efficiency: 0.618
8 threads, 27.987 seconds. Speedup: 3.259, Efficiency: 0.407

```

### Counting Sort
```
Problem size: 500000000
1 threads, 5.729 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 7.394 seconds. Speedup: 0.774, Efficiency: 0.387
4 threads, 4.217 seconds. Speedup: 1.358, Efficiency: 0.339
8 threads, 1.349 seconds. Speedup: 4.246, Efficiency: 0.530

Problem size: 1000000000
1 threads, 11.577 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 12.985 seconds. Speedup: 0.891, Efficiency: 0.445
4 threads, 9.162 seconds. Speedup: 1.263, Efficiency: 0.315
8 threads, 2.551 seconds. Speedup: 4.538, Efficiency: 0.567

Problem size: 1200000000
1 threads, 13.779 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 15.577 seconds. Speedup: 0.884, Efficiency: 0.442
4 threads, 9.977 seconds. Speedup: 1.381, Efficiency: 0.345
8 threads, 3.165 seconds. Speedup: 4.353, Efficiency: 0.544

Problem size: 1500000000
1 threads, 17.267 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 22.046 seconds. Speedup: 0.783, Efficiency: 0.391
4 threads, 12.442 seconds. Speedup: 1.387, Efficiency: 0.346
8 threads, 3.803 seconds. Speedup: 4.540, Efficiency: 0.567

```

### Quick Sort
```
Problem size: 100000000
1 threads, 22.260 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 20.989 seconds. Speedup: 1.060, Efficiency: 0.530
4 threads, 15.785 seconds. Speedup: 1.410, Efficiency: 0.352
8 threads, 7.058 seconds. Speedup: 3.153, Efficiency: 0.394

Problem size: 150000000
1 threads, 34.296 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 17.942 seconds. Speedup: 1.911, Efficiency: 0.955
4 threads, 14.122 seconds. Speedup: 2.428, Efficiency: 0.607
8 threads, 16.407 seconds. Speedup: 2.090, Efficiency: 0.261

Problem size: 200000000
1 threads, 45.970 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 37.964 seconds. Speedup: 1.210, Efficiency: 0.605
4 threads, 21.419 seconds. Speedup: 2.146, Efficiency: 0.536
8 threads, 27.028 seconds. Speedup: 1.700, Efficiency: 0.212

Problem size: 500000000
1 threads, 119.472 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 118.029 seconds. Speedup: 1.012, Efficiency: 0.506
4 threads, 54.388 seconds. Speedup: 2.196, Efficiency: 0.549
8 threads, 56.185 seconds. Speedup: 2.126, Efficiency: 0.265

```

### Selection Sort
```
Problem size: 120000
1 threads, 26.587 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 13.668 seconds. Speedup: 1.945, Efficiency: 0.972
4 threads, 7.563 seconds. Speedup: 3.515, Efficiency: 0.878
8 threads, 4.585 seconds. Speedup: 5.798, Efficiency: 0.724

Problem size: 200000
1 threads, 73.434 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 37.578 seconds. Speedup: 1.954, Efficiency: 0.977
4 threads, 19.896 seconds. Speedup: 3.690, Efficiency: 0.922
8 threads, 11.362 seconds. Speedup: 6.463, Efficiency: 0.807

Problem size: 250000
1 threads, 114.999 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 58.597 seconds. Speedup: 1.962, Efficiency: 0.981
4 threads, 30.580 seconds. Speedup: 3.760, Efficiency: 0.940
8 threads, 17.029 seconds. Speedup: 6.753, Efficiency: 0.844

Problem size: 500000
1 threads, 458.094 seconds. Speedup: 1.000, Efficiency: 1.000
2 threads, 230.918 seconds. Speedup: 1.983, Efficiency: 0.991
4 threads, 118.325 seconds. Speedup: 3.871, Efficiency: 0.967
8 threads, 62.724 seconds. Speedup: 7.303, Efficiency: 0.912

```

