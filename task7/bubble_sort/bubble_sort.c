#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <omp.h>

#define swap(x, y) { \
	tmp = array[x]; \
	array[x] = array[y]; \
	array[y] = tmp; }

int *sort_gen_array(size_t length, int range)
{
	int *array = malloc(sizeof(int) * length);

	for (size_t i = 0; i < length; i++)
		array[i] = rand() % range;

	return array;
}

void sort_print(int *array, size_t length)
{
	if (length > 50)
		return;

	for (size_t i = 0; i < length; i++)
		printf(" %d", array[i]);
	printf("\n");
}

/*  The results are correct, but the parallelization is
 *  slow. Bubble sort needs to be split up into two loops
 *  in order to eliminate the data dependencies. Thus,
 *  the implementation below needs synchronization and
 *  hereby loses it's efficiency
 */
void bubblesort_bad(int *feld, int length)
{
	int i;
	unsigned char getauscht;

	do {
		getauscht = 0;
		#pragma omp parallel for
		for (i = 0; i < length - 1; i++)
			if (feld[i] > feld[i + 1]) {
				int temp = feld[i];
				feld[i] = feld[i + 1];
				feld[i + 1] = temp;
				getauscht = 1;
			}
	} while (getauscht);

}

void bubblesort(int *array, int length)
{
	unsigned char s = 1;
	int tmp;

	while (s)
	#pragma omp parallel private(tmp)
	{
		s = 0;

		#pragma omp for reduction(+:s)
		for (int i = 0; i < length - 1; i += 2) {
			if (array[i] > array[i + 1]) {
				swap(i, i + 1);
				s = 1;
			}
		}

		#pragma omp for reduction(+:s)
		for (int i = 1; i < length - 1; i += 2) {
			if (array[i] > array[i + 1]) {
				swap(i, i + 1);
				s = 1;
			}
		}
	}
}

int is_correctly_sorted(int *array, size_t length)
{
	for (size_t i = 0; i < length - 1; i++)
		if (array[i] > array[i + 1])
			return 0;
	return 1;
}

void measure_time(void (*func)(int *, int), int *arr, int len)
{
	double start, end;

	printf("%d elements, %d threads, starting...", len, omp_get_max_threads());
	fflush(stdout);

	start = omp_get_wtime();
	func(arr, len);
	end = omp_get_wtime();

	printf("Finished in %.3f seconds. ", end - start);

	if (is_correctly_sorted(arr, len))
		printf("Ordering: OK\n");
	else
		printf("Ordering: WRONG!\n");
}

int main(int argc, char *argv[])
{
	int *arr;
	int len = 100000;

	void (*which_bubblesort)(int *, int) = bubblesort;
	if (argc > 1 && strncmp(argv[1], "bad", 3) == 0) {
		which_bubblesort = bubblesort_bad;
		argc--;
		argv++;
	}
	if (argc > 1 && atol(argv[1]) > 0)
		len = atol(argv[1]);

	srand(omp_get_wtime());

	arr = sort_gen_array(len, 10000);
	measure_time(which_bubblesort, arr, len);
	sort_print(arr, len);
	free(arr);

	return EXIT_SUCCESS;
}
