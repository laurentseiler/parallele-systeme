#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

#define swap(x, y) { \
	tmp = array[x]; \
	array[x] = array[y]; \
	array[y] = tmp; }

int *sort_gen_array(size_t length, int range)
{
	int *array = malloc(sizeof(int) * length);

	for (size_t i = 0; i < length; i++)
		array[i] = rand() % range;

	return array;
}

void sort_print(int *array, size_t length)
{
	if (length > 50)
		return;

	for (size_t i = 0; i < length; i++)
		printf(" %d", array[i]);
	printf("\n");
}

void countingsort(int *array, size_t length, int min, int max)
{
	int i, j;

	int range = max - min + 1;
	int *count = malloc(range * sizeof(int));
	int *offset = malloc(range * sizeof(int));
	int *mycount;

	#pragma omp parallel private(mycount,i,j)
	{
		mycount = malloc(range * sizeof(int));

		#pragma omp for
		for (i = 0; i < range; i++) {
			count[i] = 0;
			mycount[i] = 0;
		}

		// Count number occurrence privately, then perform a manual reduction
		#pragma omp for
		for (i = 0; i < length; i++) {
			mycount[array[i] - min]++;
		}

		// The reduction
		#pragma omp critical
		for (i = 0; i < range; i++) {
			count[i] += mycount[i];
		}

		// NOTE: 'critical' has NO implicit barrier, whereas 'single' does
		#pragma omp barrier

		// Calculate number offsets in advance, so we can parallelize the array generation later
		#pragma omp single
		{
			offset[0] = 0;
			for (i = 1; i < range; i++) {
				offset[i] = offset[i - 1] + count[i - 1];
			}
		}

		// Generate array in parallel thanks to offset
		#pragma omp for
		for (i = 0; i < range; i++) {
			for (j = offset[i]; j < offset[i] + count[i]; j++) {
				array[j] = i;
			}
		}

		free(mycount);
	}

	free(count);
	free(offset);
}

int is_correctly_sorted(int *array, size_t length)
{
	for (size_t i = 0; i < length - 1; i++)
		if (array[i] > array[i + 1])
			return 0;
	return 1;
}

void measure_time(void (*func)(int *, size_t, int, int), int *arr, size_t len, int min, int max)
{
	double start, end;

	printf("%lu elements, %d threads, starting...", len, omp_get_max_threads());
	fflush(stdout);

	start = omp_get_wtime();
	func(arr, len, min, max);
	end = omp_get_wtime();

	printf("Finished in %.3f seconds. ", end - start);

	if (is_correctly_sorted(arr, len))
		printf("Ordering: OK\n");
	else
		printf("Ordering: WRONG!\n");
}

int main(int argc, char *argv[])
{
	int *arr;
	int min = 0;
	int max = 10;
	size_t len = 1000000000;
	if (argc > 1 && atol(argv[1]) > 0)
		len = atol(argv[1]);

	srand(omp_get_wtime());

	arr = sort_gen_array(len, max - min);
	measure_time(countingsort, arr, len, min, max);
	sort_print(arr, len);
	free(arr);

	return EXIT_SUCCESS;
}

