#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

#define swap(x, y) { \
	tmp = array[x]; \
	array[x] = array[y]; \
	array[y] = tmp; }

struct bucket {
	int *arr;
	size_t sz;
};

int *sort_gen_array(size_t length, int range)
{
	int *array = malloc(sizeof(int) * length);

	for (size_t i = 0; i < length; i++)
		array[i] = rand() % range;

	return array;
}

void sort_print(int *array, size_t length)
{
	if (length > 50)
		return;

	for (size_t i = 0; i < length; i++)
		printf(" %d", array[i]);
	printf("\n");
}

int cmp_int(const void *a, const void *b)
{
	int x = *((int *) a);
	int y = *((int *) b);
	return x - y;
}

void insert_into_bucket(struct bucket *b, int val)
{
	b->sz++;
	b->arr = realloc(b->arr, b->sz * sizeof val);
	b->arr[b->sz - 1] = val;
}

void bucketsort(int *array, int length, size_t num_buckets, int range)
{
	struct bucket buckets[num_buckets];
	for (int i = 0; i < num_buckets; i++) {
		buckets[i].sz = 0;
		buckets[i].arr = NULL;
	}
	size_t bucket_range = range / num_buckets;
	if (range % num_buckets != 0)
		bucket_range += 1;

	for (size_t i = 0; i < length; i++) {
		size_t target = array[i] / bucket_range;
		insert_into_bucket(&buckets[target], array[i]);
	}

	size_t offset[num_buckets];

	#pragma omp parallel
	{
		// Sort the buckets in parallel
		#pragma omp for
		for (int i = 0; i < num_buckets; i++) {
			qsort(buckets[i].arr, buckets[i].sz, sizeof(int), cmp_int);
		}

		// Calculate offset for each bucket
		#pragma omp single
		{
			offset[0] = 0;
			for (int i = 1; i < num_buckets; i++) {
				offset[i] = offset[i - 1] + buckets[i - 1].sz;
			}
		}

		// Concatenate bucket contents in result array
		#pragma omp for
		for (int i = 0; i < num_buckets; i++) {
			for (int j = 0; j < buckets[i].sz; j++) {
				int pos = offset[i] + j;
				array[pos] = buckets[i].arr[j];
			}
		}
	}

	// Run, memory, run!!
	for (size_t i = 0; i < num_buckets; i++) {
		free(buckets[i].arr);
	}
}

int is_correctly_sorted(int *array, size_t length)
{
	for (size_t i = 0; i < length - 1; i++)
		if (array[i] > array[i + 1])
			return 0;
	return 1;
}

void measure_time(void (*func)(int *, int, size_t, int), int *arr, size_t len, size_t num_buckets, int range)
{
	double start, end;

	printf("%lu elements, %d threads, starting...", len, omp_get_max_threads());
	fflush(stdout);

	start = omp_get_wtime();
	func(arr, len, num_buckets, range);
	end = omp_get_wtime();

	printf("Finished in %.3f seconds. ", end - start);

	if (is_correctly_sorted(arr, len))
		printf("Ordering: OK\n");
	else
		printf("Ordering: WRONG!\n");
}

int main(int argc, char *argv[])
{
	int *arr;
	size_t len = 200000000;
	size_t num_buckets = 32;
	int range = 1000;
	if (argc > 1 && atol(argv[1]) > 0)
		len = atol(argv[1]);

	srand(omp_get_wtime());

	arr = sort_gen_array(len, range);
	measure_time(bucketsort, arr, len, num_buckets, range);
	sort_print(arr, len);
	free(arr);

	return EXIT_SUCCESS;
}
