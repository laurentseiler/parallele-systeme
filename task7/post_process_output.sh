#!/bin/bash

#
# USAGE:
#
# Either './post_process_output.sh output/algorithm_sort.out'
# or via stdin: 'cat output/algorithm_sort.out | ./post_process_output.sh'
#
# Hint: The script does NOT change the given input file!
#

OLDIFS=$IFS
IFS=$'\n'

while read line
do
        if [[ $line == Problem* ]]
        then
		sz=$(echo $line | sed 's/[^0-9]*//')
		echo $line

	elif [[ $line == $sz* ]]
	then
		threads=$(echo $line | sed -r 's/.* ([0-9]+) threads.*/\1/')
		seconds=$(echo $line | sed -r 's/.* ([0-9]+\.[0-9]+) seconds.*/\1/')

		if [[ $threads -eq 1 ]]
		then
			base=$seconds
		fi

		speedup="$(dc <<< "3 k $base $seconds / p")"
		efficiency="$(dc <<< "3 k $speedup $threads / p")"

		if [[ $speedup == .* ]]
		then
			speedup="0$speedup"
		fi

		if [[ $efficiency == .* ]]
		then
			efficiency="0$efficiency"
		fi

		line="$(echo "$line" | sed 's/^.* elements, //' | sed 's/, starting\.\.\.Finished in /, /' | sed 's/ Ordering: OK//')"
		echo $line Speedup: $speedup, Efficiency: $efficiency

	else
		echo $line
        fi
done < "${1:-/dev/stdin}"

