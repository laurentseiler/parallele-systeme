#!/bin/bash

echo '# Parallel Systems - Homework 7'
echo
echo '## Usage'
echo 'Copy all the files to the LCC. On the server, just execute `./run_all.sh`,'
echo 'which will compile the sources and add the SGE scripts to the queue.'
echo
echo 'When all computations have finished (this takes several hours, check with'
echo '`qstat -u $USER`), copy the `output/` folder back to your local machine.'
echo
echo 'The files in output now contain individual measurement data. To add speedup'
echo 'and efficiency measurements and generate this documentation, simply execute'
echo '`./generate_readme.sh > README.md`.'
echo
echo
echo '## Performance measurement results'
echo
for res in output/*.out
do
	echo "### $res" | sed 's/output\/\(.*\).out/\1/' | tr '_' ' ' | sed -r 's/\<./\U&/g'
	echo '```'
	./post_process_output.sh $res
	echo '```'
	echo
done
