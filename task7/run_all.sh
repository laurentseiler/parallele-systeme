#!/bin/bash

# Call this script on the server to compile all the algorithms
# and add them to the SGE.
#
# The results can be found in  the task7/output/ directory.
# Use the 'post_process.sh' script (also in this directory)
# to get speedup and efficiency values.

algos="bubble_sort bucket_sort counting_sort quick_sort selection_sort"

for algo in $algos
do
	cd $algo
	make clean all
	res=$?
	cd -
	if [[ $res -ne 0 ]]
	then
		echo Building $algo failed, aborting...
		exit 1
	fi
done

mkdir -p output
cd output
rm -f *.out
cd -

cd sge_files
for algo in *.sge
do
	qsub $algo
done
cd -

qstat -u $USER

