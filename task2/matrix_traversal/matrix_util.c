#include <stdlib.h>
#include "matrix_util.h"

double *gen_matrix(size_t row, size_t col)
{
    double *m = malloc(row * col * sizeof(double));

    for (size_t i = 0; i < row; i++) {
        for (size_t j = 0; j < col; j++) {
            m[col * i + j] = 0;
        }
    }

    return m;
}

void row_wise(double *m, size_t row, size_t col)
{
    double dummy_read;

    for (size_t i = 0; i < row; i++) {
        for (size_t j = 0; j < col; j++) {
            dummy_read =  m[col * i + j];
        }
    }
}

void col_wise(double *m, size_t row, size_t col)
{
    double dummy_read;

    for (size_t i = 0; i < col; i++) {
        for (size_t j = 0; j < row; j++) {
            dummy_read = m[row * j + i];
        }
    }
}
