#ifndef MATRIX_UTIL_H
#define MATRIX_UTIL_H

#define ROW 2048
#define COL 2048

double *gen_matrix(size_t row, size_t col);
void row_wise(double *m, size_t row, size_t col);
void col_wise(double *m, size_t row, size_t col);

#endif // MATRIX_UTIL_H
