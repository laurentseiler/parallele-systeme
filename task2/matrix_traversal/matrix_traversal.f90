subroutine row_wise(a, dim)
  implicit none
  integer, intent(in) :: dim
  real*8, intent(in) :: a(dim, dim)
  integer i, j
  real*8 dummy

  do 10 i = 1, dim
    do 10 j = 1, dim
      dummy = a(i, j)
  10 continue
end subroutine row_wise


subroutine col_wise(a, dim)
  implicit none
  integer, intent(in) :: dim
  real*8, intent(in) :: a(dim, dim)
  integer i, j
  real*8 dummy

  do 10 j = 1, dim
    do 10 i = 1, dim
      dummy = a(i, j)
  10 continue
end subroutine col_wise


program matrix
  implicit none
  integer, parameter :: dim = 2048
  integer i, j
  real*8 start, finish
  real*8 a(dim, dim)

  do 10 i = 1, dim
    do 10 j = 1, dim
      a(i,j) = 0.01 * i + j
  10 continue

  call cpu_time(start)
  call row_wise(a, dim)
  call cpu_time(finish)
  print '("Row-wise: ", f10.8, " seconds")', finish-start

  call cpu_time(start)
  call col_wise(a, dim)
  call cpu_time(finish)
  print '("Column-wise: ", f10.8, " seconds")', finish-start
end

