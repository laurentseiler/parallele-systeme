# Parallel Systems - Homework 2 - Part 1
# Matrix traversal


*Authors:*
Michael Wolf,
Michael Huber,
Jodok Huber,
Nikolaus Rauch,
Laurent Seiler


## 1. Execution times

Simply run both programs!

- Column-wise: **0.044929**
- Row-wise:    **0.009221**

## 2. Valgrind: Cache misses

### Setup

We set up a Valgrind virtual machine with the following parameters:

- L1: 8192 byte, 2-way assoc, 64 byte cache lines
    - For both instruction and data L1
- L2: 32768 byte, 8-way assoc, 64 byte cache lines

Compilation is done without optimizations (`-O0`), debug flags (`-g`) and disabled "variable unused" warning for convenience (`-Wno-unused-but-set-variable`).

### Execution

We execute `cachegrind` on the resulting programs.

- `EMACHINE = --I1=8192,2,64 --D1=8192,2,64 --L2=32768,8,64`
- `valgrind --tool=cachegrind $(EMACHINE) ./row_traversal`
- `valgrind --tool=cachegrind $(EMACHINE) ./col_traversal`

This will produce two `cachegrind.out.PID` files which contain the required information.

### Measuring cache misses

By executing `cg_annotate cachegrind.out.PID | tail -n 7` we get the information we require (shortened):

```
--------------------------------------------------------------------------------
        Ir I1mr ILmr         Dr    D1mr    DLmr        Dw    D1mw    DLmw  file:function
--------------------------------------------------------------------------------
58,738,701    2    2 33,564,676 524,289 524,289 4,196,357       0       0  .../matrix_util.c:row_wise


--------------------------------------------------------------------------------
        Ir I1mr ILmr         Dr      D1mr      DLmr        Dw    D1mw    DLmw  file:function
--------------------------------------------------------------------------------
58,738,701    2    2 33,564,676 4,194,336 4,194,336 4,196,357       0       0  .../matrix_util.c:col_wise
```

We get an explanation of the output from <https://courses.cs.washington.edu/courses/cse326/05wi/valgrind-doc/cg_main.html> :
```
Ir : I cache reads (ie. instructions executed)
I1mr: I1 cache read misses
I2mr: L2 cache instruction read misses
Dr : D cache reads (ie. memory reads)
D1mr: D1 cache read misses
D2mr: L2 cache data read misses
Dw : D cache writes (ie. memory writes)
D1mw: D1 cache write misses
D2mw: L2 cache data write misses
```

### Results

- Total matrix size is `2048 * 2048 * 8 = 33,554,432 bytes`.
- Row-wise misses:    **524,289**
- Column-wise misses: **4,194,336** (~8 times more)

For each cache miss, the respective cache line  (i.e. `64 bytes`) is filled into L1. This explains the row-wise misses `data_size / l1_preload = 33,554,432 / 64 ~= 524,289`, after which new data needs to be loaded.

The column-wise execution misses every time, i.e. `~ 2048 * 2048 = 4,194,304` cache misses.


## 3. Fortran version

A very similar program in Fortran shows that the number of cache misses is "inverted" for row- and column-wise compared to the C version, i.e. `~ 500,000` misses column-wise against `~ 4,000,000` misses row-wise.

The reason is the way Fortran stores it's multi-dimensional arrays. Something known as "[Row-major order](https://en.wikipedia.org/wiki/Row-major_order)".

Example output:
```
--------------------------------------------------------------------------------
         Ir I1mr ILmr         Dr      D1mr      DLmr        Dw      D1mw      DLmw  file:function
--------------------------------------------------------------------------------
100,681,802   14   12 29,366,279         0         0 4,196,380 4,194,307 4,194,307  .../matrix_traversal.f90:MAIN__
 71,325,730    4    4 25,176,076 4,194,338 4,194,338 4,196,359         0         0  .../matrix_traversal.f90:row_wise_
 71,325,730    4    4 25,176,076   526,337   524,290 4,196,359         0         0  .../matrix_traversal.f90:col_wise_
```
