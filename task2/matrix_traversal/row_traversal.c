#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "matrix_util.h"

int main(int argc, char **argv)
{
    clock_t start, end;
    double *m = gen_matrix(ROW, COL);

    start = clock();
    row_wise(m, ROW, COL);
    end = clock();

    printf("TIME: Row-wise: %f\n", (end - start)/(double)CLOCKS_PER_SEC);

    return 0;
}
