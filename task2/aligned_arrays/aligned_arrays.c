#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>

#define max 1024 * 1024
#define offset 1

int main(int argc, char **argv)
{
    clock_t start, end;

    double *a = malloc(sizeof(double) * max + offset);
    double *b = malloc(sizeof(double) * max + offset);
    double *c = malloc(sizeof(double) * max + offset);
    double *d = malloc(sizeof(double) * max + offset);


    start = clock();
    for (size_t i = 0; i < max; i++) {
        a[i] = b[i] + c[i] * d[i];
    }
    end = clock();

    printf("Time with offset %d: %f\n", offset, (end-start)/(1.0 * CLOCKS_PER_SEC));

    printf("last 22 bits of a: 0x%05" PRIXPTR "\n", (uintptr_t)a & 0x3fffff);
    printf("last 22 bits of b: 0x%05" PRIXPTR "\n", (uintptr_t)b & 0x3fffff);
    printf("last 22 bits of c: 0x%05" PRIXPTR "\n", (uintptr_t)c & 0x3fffff);
    printf("last 22 bits of d: 0x%05" PRIXPTR "\n", (uintptr_t)d & 0x3fffff);

    free(a);
    free(b);
    free(c);
    free(d);

    return 0;
}
