# Parallel Systems - Homework 2 - Part 2
# Aligned Arrays


*Authors:*
Michael Wolf,
Michael Huber,
Jodok Huber,
Nikolaus Rauch,
Laurent Seiler


## 1. Execution times

Runtimes of both versions:

- Original version: **0.309625**
- Modified version:    **0.288390**

## 2. Valgrind: Cache misses

### Setup

We set up a Valgrind virtual machine with the following parameters:

- L1: 8192 byte, 2-way assoc, 64 byte cache lines
    - For both instruction and data L1
- L2: 8388608 byte, 2-way assoc, 128 byte cache lines

Compilation is done without optimizations (`-O0`), debug flags (`-g`).

### Execution

We execute `cachegrind` on the resulting programs.

- `EMACHINE = --I1=8192,2,64 --D1=8192,2,64 --L2=8388608,2,128`
- `valgrind --tool=cachegrind $(EMACHINE) ./aligned_arrays_original`
- `valgrind --tool=cachegrind $(EMACHINE) ./aligned_arrays`

This will produce two `cachegrind.out.PID` files which contain the required information.

### Measuring cache misses

By executing `cg_annotate cachegrind.out.PID` we get the information we require (shortened):

```
--------------------------------------------------------------------------------
        Ir  I1mr ILmr         Dr      D1mr      DLmr        Dw      D1mw      DLmw  file:function
--------------------------------------------------------------------------------
19,923,039    14    4 13,631,506 3,145,733 3,145,731 2,097,186 1,048,579 1,048,577  .../aligned_arrays_original.c:main


--------------------------------------------------------------------------------
        Ir  I1mr ILmr         Dr      D1mr    DLmr        Dw      D1mw   DLmw  file:function
--------------------------------------------------------------------------------
19,923,039    14    4 13,631,506 3,145,733 196,611 2,097,186 1,048,579 65,537  .../aligned_arrays.c:main

```

### Results

- Size of one array is `1024 * 1024 = 1048576`.
- Misses of original version: **3,145,731** (~15 times more)
- Misses of modified version: **196,611**

Significant 22 bits for this 2-way associative cache:

```
last 22 bits of a: 0x7A000
last 22 bits of b: 0x7B000
last 22 bits of c: 0x7B000
last 22 bits of d: 0x7B000
```

This configuration associates **3** blocks from main memory to **2** locations in the cache, resulting in a lot of misses due to this alignment and the need to read these values consecutive: `a[i] = b[i] + c[i] * d[i];`.

This is solved by adding an offset:

```
last 22 bits of a: 0x01000
last 22 bits of b: 0x03000
last 22 bits of c: 0x04000
last 22 bits of d: 0x05000
```

Running both versions on a real machine exhibits an insignificant time difference between them.

Array size: 1048576
```
Time without offset: 0.020651
Time with offset 1: 0.020513
```

Array size: 536870912
```
Time without offset: 14.231546
Time with offset 1: 14.268130
```
