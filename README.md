# Parallel Systems

## Sun Grid Engine

Example job file.

    #!/bin/sh
    #$ -cwd
    #$ -o std.out
    #$ -e std.err
    /bin/hostname

Queue job.

    $ qsub my.job

Show queue.

    $ qstat

Show available modules.

    $ module avail

## Module `pgi`

Load `pgi` module.

    $ module load pgi

The module provides many compilers and other development tools.

- `pgcc` (C compiler)
- `pgCC` (C++ compiler)
- `pgfortran` (Fortran compiler)
- `pgdbg`, `idb` (debugger)
- `pgprof` (profiler)

Compile C program using pgi compiler with profiling enabled.

    $ pgcc -Mprof prog.c
    $ ./a.out
    # A file named `pgprof.out` is created during execution
    $ pgprof pgprof.out

## Module `intel`

    $ module unload pgi
    $ module load intel
