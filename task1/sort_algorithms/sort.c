#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int *sort_gen_array(size_t length, int range)
{
    int *array = malloc(sizeof(int) * length);

    for (size_t i = 0; i < length; i++) {
        array[i] = rand() % range;
    }

    return array;
}

void sort_print(int *array, size_t length)
{
    for (size_t i = 0; i < length; i++) {
        printf(" %d", array[i]);
    }
    printf("\n");
}

void bubblesort (int *array, size_t length)
{
    int i, t, s = 1;
    while (s) {
        s = 0;
        for (i = 1; i < length; i++) {
            if (array[i] < array[i - 1]) {
                t = array[i];
                array[i] = array[i - 1];
                array[i - 1] = t;
                s = 1;
            }
        }
    }
}

void bucketsort(int *array, size_t length, size_t num_buckets)
{
    int *buckets = malloc(num_buckets * sizeof(*array));

    for (size_t i = 0; i < num_buckets; i++) {
        buckets[i] = 0;
    }

    for (size_t i = 0; i < length; i++) {
        (buckets[array[i]])++;
    }

    for (size_t i = 0, j = 0; i < num_buckets; i++) {
        for (; buckets[i] > 0; (buckets[i])--) {
            array[j++] = i;
        }
    }

    free(buckets);
}

void countingsort(int *array, size_t length, int min, int max)
{
  int i, j, z;

  int range = max - min + 1;
  int *count = malloc(range * sizeof(*array));

  for(i = 0; i < range; i++) {
      count[i] = 0;
  }

  for(i = 0; i < length; i++) {
      count[array[i] - min]++;
  }

  for(i = min, z = 0; i <= max; i++) {
    for(j = 0; j < count[i - min]; j++) {
      array[z++] = i;
    }
  }

  free(count);
}

void insertionsort(int *array, size_t length)
{
    int i, j, t;
    for (i = 1; i < length; i++) {
        t = array[i];
        for (j = i; j > 0 && t < array[j - 1]; j--) {
            array[j] = array[j - 1];
        }
        array[j] = t;
    }
}

void selectionsort(int *array, size_t length)
{
    int i, j, m, t;
    for (i = 0; i < length; i++) {
        for (j = i, m = i; j < length; j++) {
            if (array[j] < array[m]) {
                m = j;
            }
        }
        t = array[i];
        array[i] = array[m];
        array[m] = t;
    }
}

void quicksort(int *array, size_t length)
{
    int i, j, p, t;
    if (length < 2)
        return;
    p = array[length / 2];
    for (i = 0, j = length - 1;; i++, j--) {
        while (array[i] < p)
            i++;
        while (p < array[j])
            j--;
        if (i >= j)
            break;
        t = array[i];
        array[i] = array[j];
        array[j] = t;
    }
    quicksort(array, i);
    quicksort(array + i, length - i);
}

void exec_sort(int algo, size_t length, int range)
{
    srand(time(NULL));
    clock_t start, end;
    int *array = sort_gen_array(length, range);
    //sort_print(array, length);
    switch (algo) {
        case 0:
            start = clock();
            bubblesort(array, length);
            end = clock();
            printf( "Time (Bubblesort): %f\n", (end-start)/(double)CLOCKS_PER_SEC);
            break;
        case 1:
            start = clock();
            bucketsort(array, length, range);
            end = clock();
            printf( "Time (Bucketsort): %f\n", (end-start)/(double)CLOCKS_PER_SEC);
            break;
        case 2:
            start = clock();
            countingsort(array, length, 0, range);
            end = clock();
            printf( "Time (Countingsort): %f\n", (end-start)/(double)CLOCKS_PER_SEC);
            break;
        case 3:
            start = clock();
            insertionsort(array, length);
            end = clock();
            printf( "Time (Insertionsort): %f\n", (end-start)/(double)CLOCKS_PER_SEC);
            break;
        case 4:
            start = clock();
            selectionsort(array, length);
            end = clock();
            printf( "Time (Selectionsort): %f\n", (end-start)/(double)CLOCKS_PER_SEC);
            break;
        case 5:
            start = clock();
            quicksort(array, length);
            end = clock();
            printf( "Time (Quicksort): %f\n", (end-start)/(double)CLOCKS_PER_SEC);
            break;
    }
    //sort_print(array, length);
}

int main(int argc, char *argv[]) {

    /*
    * First Parameter (int):
    *   0: bubblesort
    *   1: bucketsort (kinda countingsort without min when used without some sub-sorting algorithm)
    *   2: countingsort
    *   3: insertionsort
    *   4: selectionsort
    *   5: quicksort
    *
    * Second Parameter (size_t):
    *   array length
    *
    * Third Parameter (int):
    *   upper limit of random generator
    */
    for (int i = 0; i <= 5; i++)
        exec_sort(i, 100000, 100);

    return 0;
}
