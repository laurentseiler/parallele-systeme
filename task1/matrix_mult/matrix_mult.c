#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int** matrix_gen_rnd(size_t dim_r, size_t dim_c, int range)
{
    int **m = (int **) malloc(sizeof(int *) * dim_r);

    for (size_t i = 0; i < dim_r; i++) {
        m[i] = (int *) malloc(sizeof(int) * dim_c);
    }

    for (size_t i = 0; i < dim_r; i++) {
        for (size_t j = 0; j < dim_c; j++) {
            m[i][j] = rand() % range;
        }
    }

    return m;
}

int** matrix_mult(int **m1, int **m2, size_t dim_r_1, size_t dim_c_1, size_t dim_r_2, size_t dim_c_2)
{
    if(dim_c_1 != dim_r_2) {
        printf("Dimensions error.\n");
        exit(1);
    }

    int **m = (int **) malloc(sizeof(int *) * dim_r_1);

    for (size_t i = 0; i < dim_r_1; i++) {
        m[i] = (int *) malloc(sizeof(int) * dim_c_2);
    }

    for (size_t i = 0; i < dim_r_1; i++) {
        for (size_t j = 0; j < dim_c_2; j++) {
            int sum = 0;
            for (size_t k = 0; k < dim_c_1; k++) {
                sum += m1[i][k] * m2[k][j];
            }
            m[i][j] = sum;
        }
    }

    return m;
}

void matrix_print(int **m, size_t dim_r, size_t dim_c)
{
    for (size_t i = 0; i < dim_r; i++) {
        for (size_t j = 0; j < dim_c; j++) {
            printf(" %d", m[i][j]);
        }
        printf("\n");
    }
}

void exec_matrix(size_t dim_r_1, size_t dim_c_1, size_t dim_r_2, size_t dim_c_2, int range)
{
    srand(time(NULL));
    clock_t start, end;

    int **m1 = matrix_gen_rnd(dim_r_1, dim_c_1, range);
    int **m2 = matrix_gen_rnd(dim_r_2, dim_c_2, range);

    start = clock();
    int **m3 = matrix_mult(m1, m2, dim_r_1, dim_c_1, dim_r_2, dim_c_2);
    end = clock();

    printf( "Time (Matrix multiplication): %f\n", (end-start)/(double)CLOCKS_PER_SEC);

    /*
    matrix_print(m1, dim_r_1, dim_c_1);
    printf("\n");
    matrix_print(m2, dim_r_2, dim_c_2);
    printf("\n");
    matrix_print(m3, dim_r_1, dim_c_2);
    */
}

int main(int argc, char *argv[])
{
    /*
    * First Parameter (size_t):
    *   number of rows of first matrix
    *
    * Second Parameter (size_t):
    *   number of collumns of first matrix
    *
    * Third Parameter (size_t):
    *   number of rows of second matrix
    *
    * Fourth Parameter (size_t):
    *   number of collumns of second matrix
    *
    * Fifth Parameter (int):
    *   upper limit of random generator
    */
    exec_matrix(1000, 1000, 1000, 1000, 100);

    return EXIT_SUCCESS;
}
