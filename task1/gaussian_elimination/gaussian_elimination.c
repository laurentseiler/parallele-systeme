#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include "read_matrix.h"

#define C(i, j) (m->vals[(i) * m->cols + (j)])

size_t search_k(struct matrix *m, size_t i, size_t j)
{
	for (size_t k = i + 1; k < m->rows; k++) {
		if (C(k, j) != 0)
			return k;
	}

	return 0;
}

void swap_rows(struct matrix *m, size_t i, size_t k)
{
	size_t row_len, offset_i, offset_k;
	double tmp;

	row_len = m->cols;
	offset_i = row_len * i;
	offset_k = row_len * k;

	for (size_t pos = 0; pos < row_len; pos++) {
		tmp = C(i, pos);
		m->vals[offset_i + pos] = C(k, pos);
		m->vals[offset_k + pos] = tmp;
	}
}

void multiply_row(struct matrix *m, size_t i, double inv)
{
	size_t row_len, offset;

	row_len = m->cols;
	offset = row_len * i;

	for (size_t pos = 0; pos < row_len; pos++) {
		m->vals[offset + pos] *= inv;
	}
}

void subtract_from_row(struct matrix *m, double times, size_t i, size_t l)
{
	size_t row_len, offset;
	double *i_row;

	row_len = m->cols;
	offset = row_len * l;
	i_row = m->vals + row_len * i;

	for (size_t pos = 0; pos < row_len; pos++) {
		m->vals[offset + pos] -= times * i_row[pos];
	}
}

void gaussian_elimination(struct matrix *m)
{
	size_t i, j, k, l, rows, cols, next_step;
	double inverse;

	rows = m->rows;
	cols = m->cols;

	next_step = 1;

	do {
		switch (next_step) {
		case 1:
			i = j = 0;

		case 2:
			if (C(i, j) != 0) {
				next_step = 4;
				break;
			}

			k = search_k(m, i, j);

		case 3:
			if (k == 0) {
				j++;
				next_step = 2;
				break;
			}

			swap_rows(m, i, k);
			assert(C(i, j) != 0);

		case 4:
			inverse = 1.0 / C(i, j);
			multiply_row(m, i, inverse);

		case 5:
			for (l = 0; l < rows; l++) {
				if (l == i) continue;
				if (C(l, j) == 0) continue;
				subtract_from_row(m, C(l, j), i, l);
			}

		case 6:
			i++;
			j++;
			next_step = 2;
			break;

		}

	} while (!(i >= rows || j >= cols));
}

void measure_time(void (*func)(struct matrix *), void *arg)
{
	clock_t start, end;
	double runtime;

	start = clock();
	func(arg);
	end = clock();

	runtime = (end - start) / (1.0 * CLOCKS_PER_SEC);

	printf("Execution finished in %f seconds.\n", runtime);
}

int main(int verbose, char **argv)
{
	struct matrix *m;

	--verbose;

	if ((m = read_matrix()) == NULL) {
		fprintf(stderr, "Could not read matrix!\n");
		exit(EXIT_FAILURE);
	}
	verbose && print_matrix(m, "Given matrix");
	measure_time(gaussian_elimination, m);
	verbose && print_matrix(m, "Solved matrix");
	free_matrix(m);

	return EXIT_SUCCESS;
}
