#include "read_matrix.h"
#include "getline.c"

static size_t parse_line(char *lineptr, double **arrptr, size_t row_num, size_t row_size);

struct matrix *read_matrix()
{
	char *lineptr;
	size_t n, cnt, row_num, row_size;
	int ret;
	double *arrptr;

	row_num = row_size = 0;
	arrptr = NULL;

	for (;;) {
		lineptr = NULL;
		n = 0;

		// Read in line
		ret = getline(&lineptr, &n, stdin);

		// Remove trailing newline
		if (ret > 0 && lineptr[ret - 1] == '\n') {
			ret -= 1;
			lineptr[ret] = '\0';
		}

		// Finish if empty line received
		if (ret < 1) break;

		// Parse line to numbers array
		cnt = parse_line(lineptr, &arrptr, row_num, row_size);

		// Verify that there's a constant number of columns
		if (cnt == 0) {
			fprintf(stderr, "No numbers in row or memory allocation error!\n");
			return NULL;
		} else if (row_size == 0) {
			row_size = cnt;
		} else if (row_size != cnt) {
			fprintf(stderr, "Matrix row sizes mismatch!\n");
			return NULL;
		}

		row_num++;
		free(lineptr);

	}

	// Free the last (empty) line
	free(lineptr);

	// Build return value
	struct matrix *m = malloc(sizeof(struct matrix));
	m->rows = row_num;
	m->cols = row_size;
	m->vals = arrptr;

	return m;
}

int print_matrix(struct matrix *m, const char *msg)
{
	printf("%s:\n", msg);
	for (int i = 0; i < m->rows; i++) {
		for (int j = 0; j < m->cols; j++) {
			printf("%5.1f  ", m->vals[i * m->cols + j]);
		}
		printf("\n");
	}
	printf("\n");

	return 0;
}

void free_matrix(struct matrix *m)
{
	free(m->vals);
	free(m);
}

static size_t parse_line(char *lineptr, double **arrptr, size_t row_num, size_t row_size)
{
	double num;
	size_t cnt, offset;
	char *nptr, *endptr;

	// Allocate array if num of columns is known
	if (row_num > 0)
		*arrptr = realloc(*arrptr, (row_num + 1) * row_size * sizeof(double));
	else
		*arrptr = NULL;

	cnt = 0;
	offset = row_num * row_size;
	nptr = lineptr;

	// Loop until all numbers are parsed
	for (;;) {
		// Parse next number
		num = strtod(nptr, &endptr);

		// Quit if no numbers found
		if (endptr == nptr) break;

		// Allocate memory if necessary
		if (row_num == 0)
			*arrptr = realloc(*arrptr, (cnt + 1) * sizeof(double));

		// Fail if mem alloc failed
		if (*arrptr == NULL) return 0;

		// Add number to result array
		(*arrptr)[offset + cnt] = num;

		cnt++;
		nptr = endptr;
	}

	return cnt;
}
