#ifndef READ_MATRIX_H
#define READ_MATRIX_H

#include <stdio.h>
#include <stdlib.h>

struct matrix {
	size_t rows, cols;
	double *vals;
};

struct matrix *read_matrix();
int print_matrix(struct matrix *m, const char *msg);
void free_matrix(struct matrix *m);

#endif // READ_MATRIX_H
