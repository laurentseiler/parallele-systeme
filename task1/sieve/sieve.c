#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define SIZE 1000000000

void sieve (int numbers[], size_t size) {

    for (int i = 0; i < size; i++) {
        if (numbers[i] == 0) continue;
        for (int j = (numbers[i] * 2) - 2, k = 3; j < size && j >= 0; j = (numbers[i] * k++) - 2) {
            numbers[j] = 0;
        }
    }
}

void init_numbers(int array[], size_t size) {
    for (int i = 0, v = 2; i < size; i++, v++) {
        array[i] = v;
    }
}

/*unused - and slow*/
int check_valid_prime(int n){
  for(int i = 2; i <= n / 2; i++){
      if(n % i == 0){
          return 0;
      }
  }
  return 1;
}

int main(void){

  int* numbers = malloc(sizeof(int) * SIZE);
  init_numbers(numbers, SIZE);
  clock_t start, end;

  start = clock();
  sieve(numbers, SIZE);
  end = clock();
  printf( "Time (Sieve): %f\n", (end-start)/(double)CLOCKS_PER_SEC);
  free(numbers);
	return EXIT_SUCCESS;
}
