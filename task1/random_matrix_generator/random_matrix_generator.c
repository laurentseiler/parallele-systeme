#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char **argv)
{
	int i, j;
	size_t cols, rows;

	if (argc != 3) {
		fprintf(stderr, "Usage: %s num_rows num_cols\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	rows = atoi(argv[1]);
	cols = atoi(argv[2]);

	srand(time(NULL));

	for (i = 0; i < rows; i++) {
		for (j = 0; j < cols; j++) {
			printf("%4.1f  ", (rand() % 1000) / 10.0);
		}
		printf("\n");
	}
	printf("\n");

}
