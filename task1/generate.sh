#!/bin/bash

make

readme="$(cat README_RAW.md)"

mkdir -p output
rm output/*out output/*err
cd sge_files

for f in *.sge
do
	qsub "$f"
done

while [[ $(qstat -u $USER | wc -l) -ne 0 ]]
do
	echo waiting....
	sleep 5
done

for f in *.sge
do
        rep="../output/${f/.sge/.out}"
        readme=$(echo "$readme" | sed -e "/RESULT_${f}/ {
                r $rep
                d }")
done

cd -

echo "$readme" > README.md

# Finish locally with:
# scp ${lcc}:task1/README.md . && make readme
