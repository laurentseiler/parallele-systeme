#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <limits.h>

#define NODES 20000
#define EDGES 5000000
#define MAX_DISTANCE 10
#define START_NODE 0

/* generates a random directed graph with the given number of nodes and edges */
int** generate_graph(int nodes, int edges, int max_distance){
	int** graph = (int**)malloc(sizeof(int*) * nodes);

	for(int i = 0; i < nodes; i++){
		graph[i] = (int*)malloc(sizeof(int) * nodes);
		for(int j = 0; j < nodes; j++){
			if(i==j){
				graph[i][j] = 0;
			}else{
			graph[i][j] = INT_MAX;
			}
		}
	}

	int* x;
        for(int i = 0; i < edges; i++){
		do{
			x = &graph[rand() % nodes][rand() % nodes];
		} while(*x != INT_MAX);
		*x = rand() % max_distance;
	}

	return graph;
}

void print_graph(int** graph, int nodes){
	for(int i = 0; i < nodes; i++){
		for(int j = 0; j < nodes; j++){
			if(graph[i][j] == INT_MAX){
				printf("INF\t");
			}else{
				printf("%d\t", graph[i][j]);
			}
		}
	printf("\n");
	}
}

/* returns the index of the not jet computed node with minimum distance.
 * returns -1 if there are no more uncomputed nodes or the remaining nodes are unreachable*/
int min_distance(int* distance, bool* computed_vertices, int nodes){
	int min = INT_MAX, min_index = -1;
	for(int i = 0; i < nodes; i++){
		if(distance[i] < min && computed_vertices[i] == false){
			min = distance[i];
			min_index = i;
		}
	}
	return min_index;
}

void dijkstra(int** graph, int src, int nodes){
	bool computed_vertices[nodes];
	for (int i = 0; i < nodes; i++){
		computed_vertices[i] = false;
	}

	int* distance = graph[src];

	int current_node = src;
	while (current_node != -1){
		computed_vertices[current_node] = true;

		for (int j = 0; j < nodes; j++){
			if(!computed_vertices[j] && graph[current_node][j] != INT_MAX && (distance[current_node] + graph[current_node][j]) < distance[j]){
				distance[j] = distance[current_node] + graph[current_node][j];
			}
		}
		current_node = min_distance(distance, computed_vertices, nodes);
	}
}

int main(int argc, char *argv[]){
	srand(time(NULL));
	
	int** graph = generate_graph(NODES, EDGES, MAX_DISTANCE);
	
	clock_t start, end;
	start = clock();
	
	dijkstra(graph, START_NODE, NODES);

	end = clock();
	printf( "Time: %f\n", (end-start)/(double)CLOCKS_PER_SEC);
	free(graph);
	return EXIT_SUCCESS;
}
