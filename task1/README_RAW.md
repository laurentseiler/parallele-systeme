# Parallel Systems - Homework 1
# Sequential algorithms


*Authors:*
Michael Wolf,
Michael Huber,
Jodok Huber,
Nikolaus Rauch,
Laurent Seiler

Done
----

1. Matrix multiplication: done
2. Gaussian elimination: done
3. Dijkstra: done
4. Sieve of Eratosthenes: done
5. Sorting algorithms: done


Results:
--------

### Matrix multiplication
Command: `./matrix_mult`

```
RESULT_matrix_mult.sge
```

### Gaussian elimination
Command: `./random_matrix_generator 3000 1000 | ./gaussian_elimination`

```
RESULT_gaussian_elimination.sge
```

### Dijkstra
Command: `./dijkstra`

```
RESULT_dijkstra.sge
```

### Sieve of Eratosthenes
Command: `./sieve`

```
RESULT_sieve.sge
```


### Sorting algorithms
Command: `./sort`

```
RESULT_sort.sge
```

