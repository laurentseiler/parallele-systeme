#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>

size_t SIZE = 1000000000; /* 1 billion */

void sieve (int numbers[], size_t size) {
    int sqrtS = (int) sqrt((double) size);
    for (int i = 0; i < sqrtS; i++) {
        if (numbers[i] == 0) continue;
        int c = numbers[i];
        #pragma omp parallel for schedule(static)
        for(int j = i+c; j < size; j += c) {
            numbers[j] = 0;
        }
    }
}

void init_numbers(int array[], size_t size) {
    for (int i = 0, v = 2; i < size; i++, v++) {
        array[i] = v;
    }
}

/*unused - and slow*/
int check_valid_prime(int n) {
    for(int i = 2; i <= n / 2; i++) {
        if(n % i == 0) {
            return 0;
        }
    }
    return 1;
}

int main(int argc, char **argv) {
    if (argc > 1 && atol(argv[1]) != 0)
        SIZE = atol(argv[1]);
    int* numbers = malloc(sizeof(int) * SIZE);
    init_numbers(numbers, SIZE);
    double start, end;

    start = omp_get_wtime();
    sieve(numbers, SIZE);
    end = omp_get_wtime();
    printf( "Time (Sieve): %f\n", (end-start));
    free(numbers);
    return EXIT_SUCCESS;
}
