#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

#define MAXVAL 10

int x, y, z;
int *a, *b, *c;

void matrix_mult()
{
	int i, j, k;

	#pragma omp parallel for shared(a,b,c) private(i,j,k) schedule(static)
	for (i = 0; i < x; i++)
		for (j = 0; j < z; j++)
			for (k = 0; k < y; k++)
				c[i*z + j] += a[i*y + k] * b[k*z + j];
}

void print_matrix(char *msg, int *m, int x, int y)
{
	if (x * y > 25)
		return;

	printf("%s\n", msg);
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
			printf("%4d ", m[i * y + j]);
		printf("\n");
	}
	printf("\n");
}

void measure_time(void (*func)())
{
	double start, end;

	printf("Starting. Will use %d threads.\n", omp_get_max_threads());
	printf("Matrix dimensions: %dx%d and %dx%d\n", x, y, y, z);

	start = omp_get_wtime();
	func();
	end = omp_get_wtime();

	printf("Finished in %.3f seconds.\n", end - start);
}

int main(int argc, char **argv)
{
	if (argc == 4 && atoi(argv[1]) != 0 && atoi(argv[2]) != 0 && atoi(argv[3]) != 0)
	{
		x = atoi(argv[1]);
		y = atoi(argv[2]);
		z = atoi(argv[3]);
	}
	else if (argc == 1)
	{
		x = 1000;
		y = 2000;
		z = 3000;
	}
	else
	{
		fprintf(stderr, "Invalid arguments!\n");
		fprintf(stderr, "Usage: %s dim-x dim-y dim-z\n", argv[0]);
		return EXIT_FAILURE;
	}

	a = malloc(x * y * sizeof(int));
	b = malloc(y * z * sizeof(int));
	c = malloc(x * z * sizeof(int));

	srand(time(NULL));

	for (int i = 0; i < x * y; i++)
		a[i] = rand() % MAXVAL;
	for (int i = 0; i < y * z; i++)
		b[i] = rand() % MAXVAL;
	for (int i = 0; i < x * z; i++)
		c[i] = 0;

	measure_time(matrix_mult);

	print_matrix("Matrix A:", a, x, y);
	print_matrix("Matrix B:", b, y, z);
	print_matrix("Matrix C:", c, x, z);

	free(a);
	free(b);
	free(c);

	return EXIT_SUCCESS;
}
