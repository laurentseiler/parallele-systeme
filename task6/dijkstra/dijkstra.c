#include <common.h>

struct min {
	int idx, dist;
};

struct min find_my_min(struct graph *g, bool *computed_vertices, int s, int e)
{
	int min = INT_MAX,
	    min_index = -1,
	    from = g->start_node;

	for (int to = s; to < e; to++) {

		int this_dist = get_dist(g, from, to);

		if (this_dist < min && computed_vertices[to] == false) {
			min = this_dist;
			min_index = to;
		}
	}

	return (struct min) { min_index, min };
}

void dijkstra(struct graph *g)
{
	// Remember which nodes we have already visited
	bool computed_vertices[g->num_nodes];
	memset(computed_vertices, 0, sizeof computed_vertices);

	// Run as long as there are un-visited nodes
	int from = g->start_node;
	int curr_node = g->start_node;

	// Shared mem to hold global minimum
	struct min global_min;

	#pragma omp parallel
	{
		// Every thread gets a chunk (s = start, e = end of that chunk)
		int chunk_sz = g->num_nodes / omp_get_num_threads();
		int s = omp_get_thread_num() * chunk_sz;
		int e = s + chunk_sz;
		if (omp_get_thread_num() == omp_get_num_threads())
			e = g->num_nodes;

		// Run until we have visited all reachable nodes
		while (curr_node != -1) {

			// Mark current node as visited and reset global min
			#pragma omp single
			{
				computed_vertices[curr_node] = true;
				global_min = (struct min) { -1, INT_MAX };
			}

			// For all the neighbours in my chunk, check if there is a new shortest path
			for (int to = s; to < e; to++) {

				if (computed_vertices[to])
					continue;

				int old_dist = get_dist(g, from, to);
				int new_dist_1 = get_dist(g, from, curr_node);
				int new_dist_2 = get_dist(g, curr_node, to);
				int new_dist = new_dist_1 + new_dist_2;

				if (new_dist_1 != INT_MAX &&
				    new_dist_2 != INT_MAX &&
				    new_dist < old_dist)
				{
					// We found a new shortest path!
					update_dist(g, from, to, new_dist);
				}
			}

			// Synchronize threads before calculating minima
			#pragma omp barrier

			// Compute closest un-visited next node in my chunk
			struct min my_candidate;
			my_candidate = find_my_min(g, computed_vertices, s, e);

			// Select the globally closest node from all threads
			#pragma omp critical
			{
				if (my_candidate.dist < global_min.dist || global_min.idx == -1) {
					global_min = my_candidate;
					curr_node = global_min.idx;
				}
			}

			// Synchronize before each iteration
			#pragma omp barrier
		}
	}
}

double measure_time(void func(struct graph *), void *g)
{
	double start, end;

	start = omp_get_wtime();
	func(g);
	end = omp_get_wtime();

	return end - start;
}

int main(int argc, char **argv)
{
	struct graph g;
	double runtime;

	srand(time(NULL));
	parse_cmdline_args(&g, argc, argv);

	printf("Generating graph. Will need approx %.1f Mb of memory...\n",
	       (sizeof(struct edge) * g.num_nodes * g.num_nodes * g.interconnection * 1.6) / 1000000);
	generate_graph(&g);
	print_graph(&g);

	printf("Executing Dijkstra using %d threads...\n", omp_get_max_threads());
	runtime = measure_time(dijkstra, &g);

	print_graph(&g);
	free_graph(&g);
	printf("Done in %.3f seconds.\n", runtime);

	return EXIT_SUCCESS;
}
