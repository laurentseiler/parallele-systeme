#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <time.h>
#include <stdbool.h>
#include <omp.h>
#include <includes/uthash.h>

#define MAX_DIST 100

struct edge {
	int id;
	int dist;
	UT_hash_handle hh;
};

struct graph {
	struct edge *edges;
	int *edges_test;
	int num_nodes;
	double interconnection;
	int start_node;
};

#include <includes/cmdline_args.h>
#include <includes/graphlib.h>

#endif // COMMON_H
