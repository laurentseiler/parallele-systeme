#include <includes/graphlib.h>

static int hash_id(int from, int to)
{
	if (to < from) {
		int tmp = to;
		to = from;
		from = tmp;
	}

	return from + (to << 16);
}

static void add_edge(struct graph *g, int from, int to, int dist)
{
	struct edge *this_edge;

	// Something we don't need to save
	if (from == to)
		return;

	// Create the edge object
	this_edge = malloc(sizeof(struct edge));
	this_edge->id = hash_id(from, to);
	this_edge->dist = dist;

	// Add edge object to dict
	HASH_ADD_INT(g->edges, id, this_edge);
}

int get_dist(struct graph *g, int from, int to)
{
	int id;
	struct edge *res;

	// Something we always know
	if (from == to)
		return 0;

	// Look it up!
	id = hash_id(from, to);
	HASH_FIND_INT(g->edges, &id, res);

	// Return appropriate edge distance, if any
	if (res)
		return res->dist;
	return INT_MAX;
}

void update_dist(struct graph *g, int from, int to, int dist)
{
	int id;
	struct edge *e;

	// The usual check
	if (from == to)
		return;

	// Look if we already have this edge
	id = hash_id(from, to);
	HASH_FIND_INT(g->edges, &id, e);

	if (e) {
		// It's there. Update distance
		e->dist = dist;
	} else {
		// Insert a new edge
		add_edge(g, from, to, dist);
	}
}

void generate_graph(struct graph *g)
{
	// Initialize uthash data structure
	g->edges = NULL;

	for (int i = 0; i < g->num_nodes; i++) {

		// Connect roughly 'interconnection' edges
		int conn_num = g->num_nodes * g->interconnection;

		for (int j = 0; j < conn_num; j++) {
			int target = rand() % g->num_nodes;
			add_edge(g, i, target, rand() % MAX_DIST);
		}
	}
}

void print_graph(struct graph *g)
{
	/* not implemented */
}

void free_graph(struct graph *g)
{
	struct edge *curr, *tmp;

	HASH_ITER(hh, g->edges, curr, tmp) {
		HASH_DEL(g->edges, curr);
		free(curr);
	}
}

