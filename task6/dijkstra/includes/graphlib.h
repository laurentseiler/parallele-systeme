#ifndef GRAPHLIB_H
#define GRAPHLIB_H

#include <common.h>

int get_dist(struct graph *g, int from, int to);
void update_dist(struct graph *g, int from, int to, int dist);
void generate_graph(struct graph *g);
void print_graph(struct graph *g);
void free_graph(struct graph *g);

#endif // GRAPHLIB_H
