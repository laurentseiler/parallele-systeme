# Tutorial 2

Begin with `module load pgi/10.5`.

Documentation of PGI: <http://pgroup.com/doc/pgiug.pdf>

Compile program with `pgcc -Mconcur -Minfo -o program program.c`,
it will try to auto-magically parallelize a sequential program.

*Error:* `/usr/bin/ld` cannot find `-lnuma`, so search for `libnuma`
using `locate libnuma.`. Solution: `module load pgi/10.5`.

According to the documentation, do we need to set the number of
processes? Yes: `NCPUS`.

Read documentation:
<http://www.uibk.ac.at/zid/systeme/hpc-systeme/common/tutorials/sge-howto.html>

For Intel compiler, unload PGI with `module unload pgi` and use `module load intel`.

