#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int some_func()
{
	return rand() % 1000;
}

int main(int argc, char **argv)
{
	int i,
	    j,
	    s = 0,
	    n = 1000000,
	    n2 = 10000,
	    n3 = 10000;
	clock_t start,
		end;
	int *arr = malloc(n * sizeof(int));
	int **arr2 = malloc(n2 * sizeof(int *));
	int **arr3 = malloc(n3 * sizeof(int *));

	srand(time(NULL));


	/* Loop 1 */
	start = clock();

	for (i = 0; i < n; i++)
		arr[i] = i;

	end = clock();
	printf("First loop done in %f seconds.\n", s, (end - start * 1.0) / CLOCKS_PER_SEC);

	free(arr);


	/* Loop 2 */
	start = clock();

	for (i = 0; i < n2; i++)
	{
	#pragma cncall
		arr2[i] = malloc(n2 * sizeof(int));
		for (j = 0; j < n2; j++)
			arr2[i][j] = i + j;
	}

	end = clock();
	printf("Second loop done in %f seconds.\n", s, (end - start * 1.0) / CLOCKS_PER_SEC);

	for (i = 0; i < n2; i++)
		free(arr2[i]);
	free(arr2);


	/* Loop 3 */
	start = clock();

	for (i = 0; i < n3; i++)
	{
		arr3[i] = malloc(n3 * sizeof(int));
		for (j = 0; j < n3; j++)
			arr3[i][j] = i + j;
	}

	end = clock();
	printf("Third loop done in %f seconds.\n", s, (end - start * 1.0) / CLOCKS_PER_SEC);

	for (i = 0; i < n3; i++)
		free(arr3[i]);
	free(arr3);


	/* Prevent dead code elimination */
	for (i = 0; i < n; i++)
		s += i;
	printf("Result: %d", s, (end - start * 1.0) / CLOCKS_PER_SEC);

	return EXIT_SUCCESS;
}
