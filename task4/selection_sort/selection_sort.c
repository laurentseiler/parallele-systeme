#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>
#include <mpi.h>

#define swap(x, i, j) { \
	int tmp = (x)[(i)]; \
	(x)[(i)] = (x)[(j)]; \
	(x)[(j)] = tmp; }

#define min(x, y) \
	((x) < (y)) ? (x) : (y);

#define max(x, y) \
	((x) > (y)) ? (x) : (y);

#define tell_pos() \
	printf("  Thread %d in line %d. Range=[%2lu, %2lu), chunk_sz=%lu, " \
		"local_min=%3d@pos%2d.\n", \
		rank, __LINE__, start, end, chunk_sz, local_min[VAL], local_min[POS]);

#define VAL 0
#define POS 1

int size, rank;

int *gen_array(size_t arr_sz)
{
	if (rank > 0)
		return NULL;

	srand(time(NULL));

	int *arr = malloc(arr_sz * sizeof(int));

	for (int i = 0; i < arr_sz; i++)
		arr[i] = rand() % 1000;

	return arr;
}

int *gen_buffer(size_t buf_sz)
{
	return calloc(buf_sz, sizeof(int));
}

void print_array(int *arr, size_t arr_sz)
{
	if (arr_sz > 20)
		return;

	printf("\n");
	for (int i = 0; i < arr_sz; i++)
		printf("%d  ", arr[i]);
	printf("\n");
}

size_t calc_bufsiz(size_t arr_sz)
{
	size_t sz;

	sz = arr_sz / size;
	if (arr_sz % size != 0)
		sz++;

	return sz;
}

void find_local_min(int *buf, size_t chunk_sz, size_t offset, int *res)
{
	res[POS] = 0;
	res[VAL] = INT_MAX;

	for (int i = 0; i < chunk_sz; i++)
	{
		if (buf[i] < res[VAL])
		{
			res[POS] = i;
			res[VAL] = buf[i];
		}
	}

	res[POS] += offset;
}

void selection_sort(int *arr, size_t arr_sz)
{
	size_t start, end, chunk_sz;
	size_t buf_sz = calc_bufsiz(arr_sz);
	int *buf = gen_buffer(buf_sz);
	int local_min[2], overall_min[2];

	for (int pos = 0; pos < arr_sz - 1; pos++)
	{
		buf_sz = calc_bufsiz(arr_sz - pos);

		start = min(pos + (rank * buf_sz), arr_sz);
		end = min(start + buf_sz, arr_sz);
		chunk_sz = end - start;

		MPI_Scatter(arr + pos, buf_sz, MPI_INT, buf, buf_sz, MPI_INT, 0, MPI_COMM_WORLD);

		find_local_min(buf, chunk_sz, start, local_min);

		MPI_Reduce(local_min, overall_min, 1, MPI_2INT, MPI_MINLOC, 0, MPI_COMM_WORLD);

		if (rank == 0 && overall_min[POS] != pos)
			swap(arr, pos, overall_min[POS]);
	}

	free(buf);
}

void measure_time(void func(int *, size_t), int *arr, size_t arr_sz)
{
	if (rank > 0)
	{
		func(arr, arr_sz);
		return;
	}

	double start, end;

	start = MPI_Wtime();
	func(arr, arr_sz);
	end = MPI_Wtime();

	print_array(arr, arr_sz);

	printf("%.3f seconds.\n", end - start);
}

int main(int argc, char **argv)
{
	MPI_Init(&argc, &argv);

	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	/* arr_sz 250000 ~  81 seconds */
	/* arr_sz 300000 ~ 116 seconds */
	size_t arr_sz = 20;
	int *arr;

	if (argc > 1 && atol(argv[1]) != 0)
		arr_sz = atoi(argv[1]);

	arr = gen_array(arr_sz);
	measure_time(selection_sort, arr, arr_sz);
	free(arr);

	MPI_Finalize();

	return EXIT_SUCCESS;
}
