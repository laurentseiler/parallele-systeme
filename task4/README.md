# Parallel Systems - Homework 4


*Authors:*
Michael Wolf,
Michael Huber,
Jodok Huber,
Nikolaus Rauch,
Laurent Seiler

Done
----

_Everything:_ 30 / 30 points


Results:
--------

### Counting sort
Command: `qstat counting_sort.sge`

```
Problem size: 400.000.000 Range: 1.000
1  Core: 5.914472 seconds.
2  Cores: 5.168880 seconds.
4  Cores: 4.871296 seconds.
8  Cores: 4.675831 seconds.
16 Cores: 10.462579 seconds.

Problem size: 800.000.000 Range: 1.000
1  Core: 11.798381 seconds.
2  Cores: 10.501031 seconds.
4  Cores: 9.773920 seconds.
8  Cores: 9.330299 seconds.
16 Cores: 20.959520 seconds.

Problem size: 1.600.000.000 Range: 1.000
1  Core: 23.551592 seconds.
2  Cores: 20.583498 seconds.
4  Cores: 19.567449 seconds.
8  Cores: 19.035721 seconds.
16 Cores: 41.753353 seconds.
```

### Bucket sort
Command: `qstat bucket_sort.sge`

```
Problem size: 40.000.000 Range: 1.000
1  Core: 12.828189 seconds.
2  Cores: 7.275631 seconds.
4  Cores: 4.823930 seconds.
8  Cores: 3.953789 seconds.
16 Cores: 5.238151 seconds.

Problem size: 80.000.000 Range: 1.000
1  Core: 26.209763 seconds.
2  Cores: 14.761867 seconds.
4  Cores: 9.843073 seconds.
8  Cores: 8.146507 seconds.
16 Cores: 10.505399 seconds.

Problem size: 160.000.000 Range: 1.000
1  Core: 53.789872 seconds.
2  Cores: 30.191808 seconds.
4  Cores: 20.083478 seconds.
8  Cores: 16.363369 seconds.
16 Cores: 21.042418 seconds.
```

### Selection sort
Command: `qstat selection_sort.sge`

```
Problem size: 300000
1  Cores: 191.777 seconds.
2  Cores: 194.321 seconds.
4  Cores: 195.675 seconds.
8  Cores: 183.461 seconds.
16 Cores: 1044.492 seconds.

Problem size: 350000
1  Cores: 261.290 seconds.
2  Cores: 264.438 seconds.
4  Cores: 265.963 seconds.
8  Cores: 245.611 seconds.
16 Cores: 1479.382 seconds.

Problem size: 400000
1  Cores: 341.473 seconds.
2  Cores: 344.470 seconds.
4  Cores: 346.898 seconds.
8  Cores: 318.957 seconds.
16 Cores: 1963.527 seconds.
```

**Note:** A version without `MPI_Scatter` performs significantly faster!

Command: `qstat selection_sort_old.sge`

```
Problem size: 300000
1  Cores: 183.172 seconds.
2  Cores: 92.113 seconds.
4  Cores: 47.145 seconds.
8  Cores: 24.752 seconds.
16 Cores: 49.134 seconds.

Problem size: 350000
1  Cores: 248.773 seconds.
2  Cores: 125.231 seconds.
4  Cores: 63.795 seconds.
8  Cores: 33.867 seconds.
16 Cores: 55.440 seconds.

Problem size: 400000
1  Cores: 324.869 seconds.
2  Cores: 164.096 seconds.
4  Cores: 83.167 seconds.
8  Cores: 43.306 seconds.
16 Cores: 66.010 seconds.
```


