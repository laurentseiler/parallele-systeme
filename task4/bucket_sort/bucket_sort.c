#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

typedef struct {
    int min;
    int max;
    int size;
    int* values;
} Bucket;

static int cmp_int(const void *a, const void *b) {
    const int *ia = (const int *)a; // casting pointer types
    const int *ib = (const int *)b;
    return *ia  - *ib;
}

Bucket* init_buckets(int p_num, int size, int range){
	Bucket* buckets = malloc(sizeof(Bucket) * p_num);
    for(int i=0, j=0; i<p_num; i++) {
		buckets[i].values = malloc(sizeof(int) * size); 
		buckets[i].min = j;
        buckets[i].max = j > range ? range : j + range/p_num;
        j += range/p_num + 1;
        buckets[i].size = 0;
    }
	return buckets;
}

void free_buckets(Bucket* buckets, int p_num){
        for(int i=0; i<p_num; i++) {
            free(buckets[i].values);
        }
        free(buckets);
}

int bucket_number(Bucket* buckets, int b_count, int value) {
    for(int i=0; i<b_count; i++) {
        if(value >= buckets[i].min && value <= buckets[i].max) {
            return i;
        }
    }
    return -1;
}

bool is_sorted(int* values, int size) {
    for(int i=1; i<size; i++) {
        if(values[i] < values[i-1]) {
            return false;
        }
    }

    return true;
}

int* generate_values(int size, int range) {

    int* values = malloc(sizeof(int) * size);

    for (size_t i = 0; i < size; i++) {
        values[i] = rand() % range;
    }

    return values;
}


int main(int argc, char** argv) {

    if(argc < 3) {
        fprintf(stderr, "give problemsize and max value range!\n");
        return EXIT_FAILURE;
    }
	srand(time(NULL));
	int rank, p_num;	

    MPI_Init(&argc, &argv);
   
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &p_num);

    int size = atoi(argv[1]);
    int range = atoi(argv[2]);
    int rec_size = size;
    int b_size = 0;;
    int* values = generate_values(size, range);
    int* rec = malloc(sizeof(int) * size);
    int* bucket_displacements = malloc(sizeof(int) * p_num);
    int* bucket_sizes = malloc(sizeof(int) * p_num);
	double t0;

    if(rank == 0) {
		t0 = MPI_Wtime();
		
		Bucket* buckets =  init_buckets(p_num, size, range);

        int b_num = 0, b_index = 0;
        
		//put numbers into their buckets
        for(int i=0; i<size; i++) {
            b_num = bucket_number(buckets, p_num, values[i]);
            b_index = buckets[b_num].size;

            buckets[b_num].values[b_index] = values[i];
            buckets[b_num].size++;
        }
        //flatten structure
        for(int i=0, offset=0; i<p_num; i++) {
            memcpy(&values[offset], buckets[i].values, sizeof(int)*buckets[i].size);
            bucket_displacements[i] = offset;
            offset += buckets[i].size;
        }

        

        //distribute bucket sizes - todo look for better way
        b_size = buckets[0].size;
        bucket_sizes[0] = b_size;
        for(int i=1; i<p_num; i++) {
            bucket_sizes[i] = buckets[i].size;
            MPI_Send(&buckets[i].size, 1, MPI_INT, i, 42, MPI_COMM_WORLD);
        }

		//free unused memory
		free_buckets(buckets, p_num);

    } else {
        MPI_Recv(&b_size, 1, MPI_INT, 0, 42, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    //distribute buckets
    MPI_Scatterv(values, bucket_sizes, bucket_displacements, MPI_INT, rec, rec_size,  MPI_INT, 0, MPI_COMM_WORLD);

    //sort with qsort
    qsort(rec, b_size, sizeof(int), cmp_int);

    //gather data and put it back together
    MPI_Gatherv(rec, b_size, MPI_INT, values, bucket_sizes, bucket_displacements, MPI_INT, 0, MPI_COMM_WORLD);

    if(rank == 0) {
        if(!is_sorted(values, size)) {
            fprintf(stderr, "algorithm did not work properly\n");
        }
		printf("%f seconds.\n", MPI_Wtime() - t0);
    }

    //free memory
    free(values);
    free(rec);
    free(bucket_displacements),

    MPI_Finalize();

    return EXIT_SUCCESS;
}
