#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>
#include "colors.h"

#define swap(x, i, j) { \
	int tmp = (x)[(i)]; \
	(x)[(i)] = (x)[(j)]; \
	(x)[(j)] = tmp; }

#define min(x, y) \
	((x) < (y)) ? (x) : (y);

#define tell_pos() \
	printf("%*cThread %d in line %d. Range=[%2u, %2u), my_min=%3u@pos%2u " \
		"overall_min=%3u@pos%2u.\n", pos, ' ', rank, __LINE__, start, end, \
		local_min_i[0], local_min_i[1], overall_min_i[0], overall_min_i[1]);

int size, rank;

int *gen_array(size_t arr_sz)
{
	int *arr;

	arr = malloc(arr_sz * sizeof(int));

	if (rank == 0)
	{
		srand(time(NULL));

		for (int i = 0; i < arr_sz; i++)
			arr[i] = rand() % 1000;
	}

	MPI_Bcast(arr, arr_sz, MPI_INT, 0, MPI_COMM_WORLD);

	return arr;
}

void selection_sort(int *arr, size_t arr_sz)
{
	size_t my_min_i, start, end, chunk_sz;
	int local_min_i[2], overall_min_i[2];

	for (int pos = 0; pos < arr_sz - 1; pos++)
	{
		my_min_i = pos;

		chunk_sz = ((arr_sz - pos - 1) / size) + 1;
		start = pos + 1 + (rank * chunk_sz);
		end = min(start + chunk_sz, arr_sz);

		for (int i = start; i < end; i++)
			if (arr[i] < arr[my_min_i])
				my_min_i = i;

		local_min_i[0] = arr[my_min_i];
		local_min_i[1] = my_min_i;

		MPI_Allreduce(local_min_i, overall_min_i, 1, MPI_2INT, MPI_MINLOC, MPI_COMM_WORLD);

		if (overall_min_i[1] != pos)
			swap(arr, pos, overall_min_i[1]);
	}
}

void print_array(int *arr, size_t arr_sz)
{
	if (arr_sz > 20)
		return;

	printf(TXT_DIM);
	for (int i = 0; i < arr_sz; i++)
		printf("%d  ", arr[i]);
	printf(TXT_RESET "\n");
}

void measure_time(void func(int *, size_t), int *arr, size_t arr_sz)
{
	if (rank > 0)
	{
		func(arr, arr_sz);
		return;
	}

	double start, end;

	start = MPI_Wtime();
	func(arr, arr_sz);
	end = MPI_Wtime();

	print_array(arr, arr_sz);

	printf(TXT_GREEN "%.3f " TXT_RESET "seconds.\n", end - start);
}

int main(int argc, char **argv)
{
	MPI_Init(&argc, &argv);

	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	/* arr_sz 250000 ~  81 seconds */
	/* arr_sz 300000 ~ 116 seconds */
	size_t arr_sz = 20;
	int *arr;

	if (argc > 1 && atol(argv[1]) != 0)
		arr_sz = atoi(argv[1]);

	arr = gen_array(arr_sz);
	measure_time(selection_sort, arr, arr_sz);
	free(arr);

	MPI_Finalize();

	return EXIT_SUCCESS;
}
