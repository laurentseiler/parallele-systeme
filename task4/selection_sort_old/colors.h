#ifndef COLORS_H
#define COLORS_H

#define TXT_RESET       "\033[0m"

#define TXT_NORMAL      "\x1B[0m"
#define TXT_BOLD        "\x1B[1m"
#define TXT_DIM         "\x1B[2m"
#define TXT_ITALIC      "\x1B[3m"
#define TXT_UNDERLINE   "\x1B[4m"
#define TXT_BLINK       "\x1B[5m"
#define TXT_REV_VIDEO   "\x1B[7m"
#define TXT_HIDDEN      "\x1B[8m"

#define TXT_BLACK       "\x1B[30m"
#define TXT_RED         "\x1B[31m"
#define TXT_GREEN       "\x1B[32m"
#define TXT_YELLOW      "\x1B[33m"
#define TXT_BLUE        "\x1B[34m"
#define TXT_MAGENTA     "\x1B[35m"
#define TXT_CYAN        "\x1B[36m"
#define TXT_WHITE       "\x1B[37m"

#define TXT_BG_BLACK    "\x1B[40m"
#define TXT_BG_RED      "\x1B[41m"
#define TXT_BG_GREEN    "\x1B[42m"
#define TXT_BG_YELLOW   "\x1B[43m"
#define TXT_BG_BLUE     "\x1B[44m"
#define TXT_BG_MAGENTA  "\x1B[45m"
#define TXT_BG_CYAN     "\x1B[46m"
#define TXT_BG_WHITE    "\x1B[47m"

/* Example:
 * printf(TXT_GREEN TXT_BOLD "Hello " TXT_UNDERLINE "World!\n" TXT_RESET);
 */

#endif // COLORS_H
