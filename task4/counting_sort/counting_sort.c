#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <mpi.h>

/* Sensible defaults */
#define LENGTH 10
#define RANGE 10

/* Rank of master process*/
#define MASTER 0

void print_array(int *array, size_t length)
{
	for (size_t i = 0; i < length; i++) {
		printf(" %d", array[i]);
	}
	printf("\n");
}

/* Generate random array */
int *generate_array(size_t length, int range)
{
	int *array = malloc(sizeof(int) * length);

	for (size_t i = 0; i < length; i++) {
		array[i] = rand() % range;
	}

	return array;
}

/* Count occurences in partial array */
int* count(int *partial, size_t length, int min, int max)
{
	int range = max - min + 1;
	int *occ_map = malloc(range * sizeof(int));

	for(int i = 0; i < range; i++) {
		occ_map[i] = 0;
	}

	for(int j = 0; j < length; j++) {
		occ_map[partial[j] - min]++;
	}

	return occ_map;
}

/* Use occurence map to reconstruct sorted array */
int* reconstruct(int *occ_map, size_t length, int min, int max)
{
	int i, j, k;
	int *sorted = malloc(sizeof(int) * length);

	for(i = min, k = 0; i < max; i++) {
		for(j = 0; j < occ_map[i - min]; j++) {
			sorted[k++] = i;
		}
	}

	return sorted;
}

int main(int argc, char *argv[])
{
	srand(time(NULL));
	double start, start_s, start_r;
	double end, end_s, end_r;

	int rank; /* Rank of this process */
	int size; /* Total number of processes */

	int length; /* Length of unsorted array */
	int range; /* Range of random generator from 0 to exclusive range */
	int *unsorted; /* The initial unsorted array */
	int *partial; /* Buffer for part of unsorted array per process */
	int *interm_result; /* Intermediate result calculated by each process */
	int *final_gather; /* Holding reduced intermediate results (MPI_Reduce) */
	int *sorted; /* The final sorted array */
	int accum_offset = 0; /* Accumulative offset for calculating offset map */

	if (argc == 3 && atol(argv[1]) != 0 && atol(argv[2]) != 0) {
		length = atol(argv[1]);
		range = atol(argv[2]);
	} else {
		length = LENGTH;
		range = RANGE;
	}

	MPI_Init (&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	int unev = length % size; /* Remaining elements after even scatter */
	int *size_map = malloc(sizeof(int) * size); /* Chunk size per process */
	int *offset_map = malloc(sizeof(int) * size); /* Scatter function offset */

	/* Calculate size and offset map */
	for (size_t i = 0; i < size; i++) {
		size_map[i] = length / size;

		if (unev > 0)
			size_map[i]++; unev--;

		offset_map[i] = accum_offset;
		accum_offset += size_map[i];
	}

	/* Look up process dependent buffer size in size map */
	partial = malloc(sizeof(int) * size_map[rank]);

	/* Master process initializes unsorted array and buffer for final gather */
	if (rank == MASTER) {
		unsorted = generate_array(length, range);
		final_gather = malloc(sizeof(int) * range);
#ifdef DFLAG
		print_array(unsorted, length);
#endif
		start = MPI_Wtime();
	}

	/* Scatter unsorted array according to size and offset map */
	start_s = MPI_Wtime();
	MPI_Scatterv(unsorted, size_map, offset_map, MPI_INT, partial, size_map[rank], MPI_INT, MASTER, MPI_COMM_WORLD);
	end_s = MPI_Wtime();

	/* Each process counts occurences in his partial */
	interm_result = count(partial, size_map[rank], 0, range);

	/* Reduce intermediate results to master with item-wise sum operation */
	start_r = MPI_Wtime();
	MPI_Reduce(interm_result, final_gather, range, MPI_INT, MPI_SUM, MASTER, MPI_COMM_WORLD);
	end_r = MPI_Wtime();

	/* Utilize final gather to reconstruct the array in a sorted fashion */
	if (rank == MASTER) {
#ifdef DFLAG
		print_array(final_gather, range);
#endif
		sorted = reconstruct(final_gather, length, 0, range);
#ifdef DFLAG
		print_array(sorted, length);
#endif
		end = MPI_Wtime();
		printf("Total: %fs.(Scatter: %fs Reduce: %fs)\n", end - start, end_s - start_s, end_r - start_r);
	}

	MPI_Finalize();

	if (rank == MASTER) {
		free(unsorted);
		free(final_gather);
		free(sorted);
	}

	free(partial);
	free(interm_result);

	return EXIT_SUCCESS;
}
