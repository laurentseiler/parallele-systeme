`module avail`
`module load openmpi`

Maybe need to unload PGI / Intel stuff

Use `mpicc` to compile, or
`gcc -L${UIBK_OPENMPI_LIB} -I${UIBK_OPENMPI_INC} -lmpi...`

Run MPI programs with `mpirun`

`mpirun -np 4 ./first_program`

<http://www.uibk.ac.at/zid/systeme/hpc-systeme/lcc/>

`Scatter` and `Gather` complement each other.

`MPI_WTime` for timing measurement!
