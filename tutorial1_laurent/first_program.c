#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

int main(int argc, char **argv)
{
	MPI_Init(&argc, &argv);

	int rank, size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	if (rank == 0) {
		char msg[] = "Hello World!";
		printf("Rank %d sending msg: '%s'\n", rank, msg);
		MPI_Send(msg, sizeof(msg), MPI_CHAR, rank + 1, 0, MPI_COMM_WORLD);

	} else {
		char buf[100];
		MPI_Status status;
		MPI_Recv(buf, sizeof(buf), MPI_CHAR, rank - 1, 0, MPI_COMM_WORLD, &status);
		printf("Rank %d Got msg: '%s'\n", rank, buf);

		if (rank != size - 1) {
			MPI_Send(buf, sizeof(buf), MPI_CHAR, rank + 1, 0, MPI_COMM_WORLD);
		}
	}

	MPI_Finalize();
	
	return EXIT_SUCCESS;
}
